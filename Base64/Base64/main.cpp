#include <iostream>
#include <fstream>
#include "Base64.h"
using namespace std;

int main()
{
	
	ifstream in;
	in.open("Wemos.jpg", ios::binary);
	if (!in.is_open())
	{
		cout << "File error!" << endl;
		system("pause");
		return 0;
	}
	in.seekg(0, ios::end);
	int fSize = in.tellg();
	cout << fSize;
	in.seekg(0, ios::beg);

	char* fileContent = new char[fSize];
	in.read(fileContent, fSize);

	char* encoded = base64_encode(fileContent,fSize);
	int size = strlen(encoded);
	cout << strlen(encoded) << endl;
	cout << encoded << endl;

	delete[] fileContent;
	delete[] encoded;
	in.close();

	system("pause");
	return 0;
}