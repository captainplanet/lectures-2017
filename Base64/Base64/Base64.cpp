#include "Base64.h"
#include <iostream>

char base64_chars[] =
"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
"abcdefghijklmnopqrstuvwxyz"
"0123456789+/";

char* base64_encode(char * message, int messageSize)
{
	int encodedSize = 4 * (messageSize / 3)
				+ 4 * ((bool)(messageSize % 3)) + 1;
	char* encodedStr = new char[encodedSize];
	encodedStr[encodedSize - 1] = '\0';
	unsigned char source[3];
	unsigned char indices[4];
	int counter = messageSize;
	int idx = 0;
	int encodedStrIdx = 0;
	while (counter--)
	{
		source[idx++] = *(message++);
		if (idx == 3)
		{
			idx = 0;

			indices[0] = source[0] >> 2;
			indices[1] = ((source[0] & 3) << 4)
						+ (source[1] >> 4);
			indices[2] = ((source[1] & 15) << 2)
							+ (source[2] >> 6);	
			indices[3] = source[2] & 63;

			for (int i = 0; i < 4; ++i)
				encodedStr[encodedStrIdx++] = base64_chars[indices[i]];
		}
	}

	if (idx)
	{
		for (int j = idx; j < 3; ++j)
			source[j] = '\0';

		indices[0] = source[0] >> 2;
		indices[1] = ((source[0] & 3) << 4)
			+ (source[1] >> 4);
		indices[2] = ((source[1] & 15) << 2)
			+ (source[2] >> 6);
		indices[3] = source[2] & 63;

		for (int i=0; i<idx + 1;++i)
			encodedStr[encodedStrIdx++] = base64_chars[indices[i]];

		while (idx++ < 3)
			encodedStr[encodedStrIdx++] = '=';
	}

	return encodedStr;
}
