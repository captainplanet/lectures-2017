#include <iostream>
#include <algorithm>
using namespace std;

enum Balance { d2Left = -2, dleft, ok, dright, d2Right };
enum Side { left = 0, right = 1};

template <typename Type>
struct AvlTreeNode
{
	Balance status;
	AvlTreeNode<Type>* parent;
	AvlTreeNode<Type>* left;
	AvlTreeNode<Type>* right;
	Type data;
	AvlTreeNode(Type _data) : left(nullptr),
		right(nullptr),
		parent(nullptr),
		status(Balance::ok),
		data(_data) {}
};

template <typename Type>
class AvlTree
{
private:
	AvlTreeNode<Type>* root;
	int size;
	int height;

	void addItemSubtree(Type item, AvlTreeNode<Type>* subroot);
	void deleteSubtree(AvlTreeNode<Type>* subroot);
	bool findItemSubtree(Type item, AvlTreeNode<Type>*& result, AvlTreeNode<Type>* subroot);
	void printSubtree(AvlTreeNode<Type>* subroot);

	AvlTreeNode<Type>* rotate(AvlTreeNode<Type>* subroot, Side side);
	int getHeight(AvlTreeNode<Type>* subroot);
	void setBalance(AvlTreeNode<Type>* subroot);
	Side relationSide(AvlTreeNode<Type>* parent, AvlTreeNode<Type>* child);

public:
	AvlTree() : root(nullptr), size(0), height(-1) {}
	~AvlTree();
	void addItem(Type item);
	bool removeItem(Type item);
	bool findItem(Type item, Type& result);
	void print();

};

template<typename Type>
AvlTree<Type>::~AvlTree()
{
	if (size == 0) return;
	deleteSubtree(root);
}

template<typename Type>
void AvlTree<Type>::addItem(Type item)
{
	if (size == 0)
	{
		root = new AvlTreeNode<Type>(item);
		++size;
		return;
	}
	addItemSubtree(item, root);
}

template<typename Type>
void AvlTree<Type>::print()
{
	if (size == 0) return;
	printSubtree(root);
}

template <typename Type>
bool AvlTree<Type>::findItem(Type item, Type& result)
{
	if (size == 0) return false;
	AvlTreeNode<Type>* foundNode;
	bool found = findItemSubtree(item, foundNode, root);
	if (found)
		result = foundNode->data;
	return found;
}

template <typename Type>
bool AvlTree<Type>::removeItem(Type item)
{
	AvlTreeNode<Type>* foundNode;
	bool found = findItemSubtree(item, foundNode, root);
	if (!found) return false;

	//���� ���� �������� ������
	if (foundNode->left == nullptr && foundNode->right == nullptr)
	{
		if (foundNode->parent->left == foundNode)
			foundNode->parent->left = nullptr;
		else
			foundNode->parent->right = nullptr;

		return true;
	}

	//���� � ���� ���� ��� �������
	if (foundNode->left != nullptr && foundNode->right != nullptr)
	{
		AvlTreeNode<Type>* maxNode = foundNode->left;
		while (maxNode->right != nullptr)
			maxNode = maxNode->right;
		foundNode->data = maxNode->data;
		if (maxNode->parent->left == maxNode)
			maxNode->parent->left = nullptr;
		else
			maxNode->parent->right = nullptr;
		delete maxNode;

		return true;
	}

	//���� � ���� ������������ ������� �����
	if (foundNode->left != nullptr)
	{
		if (foundNode->parent->left == foundNode)
			foundNode->parent->left = foundNode->left;
		else
			foundNode->parent->right = foundNode->left;
		delete foundNode;
	}
	//���� � ���� ������������ ������� ������
	else
	{
		if (foundNode->parent->left == foundNode)
			foundNode->parent->left = foundNode->right;
		else
			foundNode->parent->right = foundNode->right;
		delete foundNode;
	}

	return true;
}

template<typename Type>
void AvlTree<Type>::addItemSubtree(Type item, AvlTreeNode<Type>* subroot)
{
	if (item < subroot->data)
	{
		if (subroot->left == nullptr)
		{
			subroot->left = new AvlTreeNode<Type>(item);
			subroot->left->parent = subroot;
			++size;
			return;
		}
		addItemSubtree(item, subroot->left);
	}
	else
	{
		if (subroot->right == nullptr)
		{
			subroot->right = new AvlTreeNode<Type>(item);
			subroot->right->parent = subroot;
			++size;
			return;
		}
		addItemSubtree(item, subroot->right);
	}
	setBalance(subroot);
	if (subroot->status == Balance::d2Left)
	{
		if (subroot->left->status == Balance::dleft)
		{
			subroot = rotate(subroot, Side::right);
			setBalance(subroot->right);
			setBalance(subroot);
		}
		else
		{
			subroot->left = rotate(subroot->left, Side::left);
			subroot = rotate(subroot, Side::right);
			setBalance(subroot->left);
			setBalance(subroot->right);
			setBalance(subroot);
		}
	}
	else if (subroot->status == Balance::d2Right)
	{
		if (subroot->right->status == Balance::dright)
		{
			subroot = rotate(subroot, Side::left);
			setBalance(subroot->left);
			setBalance(subroot);
		}
		else
		{
			subroot->right = rotate(subroot->right, Side::right);
			subroot = rotate(subroot, Side::left);
			setBalance(subroot->left);
			setBalance(subroot->right);
			setBalance(subroot);
		}
	}
}

template<typename Type>
void AvlTree<Type>::deleteSubtree(AvlTreeNode<Type>* subroot)
{
	if (subroot->left != nullptr)
		deleteSubtree(subroot->left);
	if (subroot->right != nullptr)
		deleteSubtree(subroot->right);
	delete subroot;
}

template<typename Type>
bool AvlTree<Type>::findItemSubtree(Type item, AvlTreeNode<Type>*& result, AvlTreeNode<Type>* subroot)
{
	if (item == subroot->data)
	{
		result = subroot;
		return true;
	}
	if (item < subroot->data)
	{
		if (subroot->left == nullptr)
			return false;
		return findItemSubtree(item, result, subroot->left);
	}
	else
	{
		if (subroot->right == nullptr)
			return false;
		return findItemSubtree(item, result, subroot->right);
	}
}

template<typename Type>
void AvlTree<Type>::printSubtree(AvlTreeNode<Type>* subroot)
{
	if (subroot->left != nullptr)
		printSubtree(subroot->left);

	cout << subroot->data << endl;

	if (subroot->right != nullptr)
		printSubtree(subroot->right);
}

template<typename Type>
int AvlTree<Type>::getHeight(AvlTreeNode<Type>* subroot)
{
	if (subroot == nullptr) return -1;
	if (subroot->left == nullptr && subroot->right == nullptr)
		return 0;

	int heightRight;
	int heightLeft;

	if (subroot->left == nullptr)
		heightLeft = -1;
	else
		heightLeft = getHeight(subroot->left);

	if (subroot->right == nullptr)
		heightRight= -1;
	else
		heightRight = getHeight(subroot->right);

	return 1 + max(heightLeft, heightRight);
}

template<typename Type>
void AvlTree<Type>::setBalance(AvlTreeNode<Type>* subroot)
{
	int heightRight = getHeight(subroot->right);
	int heightLeft = getHeight(subroot->left);
	subroot->status = (Balance)(heightRight - heightLeft);
}

template<typename Type>
AvlTreeNode<Type>* AvlTree<Type>::rotate(AvlTreeNode<Type>* subroot,
										Side side)
{
	AvlTreeNode<Type>* child;
	if (side == Side::right)
	{
		child = subroot->left;
		subroot->left = child->right;
		child->right = subroot;
	}
	else
	{
		child = subroot->right;
		subroot->right = child->left;
		child->left = subroot;
	}

	if (subroot->parent != nullptr)
		if (subroot->parent->left == subroot)
			subroot->left = child;
		else
			subroot->right = child;

	return child;
}

template<typename Type>
Side AvlTree<Type>::relationSide(AvlTreeNode<Type>* parent, 
								 AvlTreeNode<Type>* child)
{
	return parent->left == child ? Side::left : Side::right;
}