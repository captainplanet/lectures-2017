#include <iostream>
#include <math.h>
#include <time.h>

#include <chrono>
using namespace std::chrono;

using namespace std;

int main()
{
	//For high resolution clock (C++11)
	high_resolution_clock::time_point sstart;
	long ms;

	//Time measurement (classic)
	clock_t start = 0;
	clock_t finish = 0;
	
	//Number of elements
	int numOfElems = 1000;
	double accuracy = 1e-4;

	//Sums for rows
	double harmonicRowSum = 0;
	double squaresRowSum = 0;
	double twoDegreesRowSum = 0;
	double piRowSum = 0;

	//Service variables
	double prevTwoDegree = 1;
	double currElement = 1;
	unsigned long long count = 0;

	cout << "Input number of elements" << endl;
	cin >> numOfElems;

	if (numOfElems <= 0)
	{
		cout << "Number must be positive" << endl;
		system("pause");
		return -1;
	}
	
	start = clock();
	sstart = high_resolution_clock::now();
	for (count = 1; count <= numOfElems; ++count)
	{
		harmonicRowSum += 1.0 / count;
		squaresRowSum += 1.0 / (count*count);
		//twoDegreesRowSum += 1.0 / pow(2, count-1);
		twoDegreesRowSum += 1.0 / prevTwoDegree;
		prevTwoDegree *= 2;
	}
	finish = clock();
	ms = duration_cast<milliseconds>(high_resolution_clock::now() - sstart).count();
	
	cout << "Harmonic Row Sum: " << harmonicRowSum << endl;
	cout << "Squares Row Sum: " << squaresRowSum << endl;
	cout << "Two Degrees Row Sum: " << twoDegreesRowSum << endl;
	cout << "Time elapsed: " << 1000.0*(finish - start) / CLOCKS_PER_SEC << endl;
	cout << "(HR)Time elapsed: " << ms << endl;

	cout << endl;
	cout << "Input accuracy for pi estimation" << endl;
	cin >> accuracy;
	
	count = 1;
	start = clock();
	while (abs(currElement) >= accuracy)
	{
		piRowSum += currElement;
		++count;
		currElement = 1.0 / (2 * count - 1);
		if (count % 2 == 0)
			currElement *= -1;
	}
	piRowSum *= 4;
	finish = clock();

	cout.precision(16);
	cout << "PI = " << piRowSum << endl;
	cout << "Time elapsed: " << 1000.0*(finish - start) / CLOCKS_PER_SEC << endl;

	//Do-While loop
	//do
	//{
	//	//
	//} while (true);

	system("pause");
	return 0;
}