#include <iostream>
#include <chrono>
#include <stdarg.h>

using namespace std;
using namespace std::chrono;

high_resolution_clock::time_point sstart;
void tic()
{
	sstart = high_resolution_clock::now();
}

long toc()
{
	return duration_cast<milliseconds>(high_resolution_clock::now() - sstart).count();
}

unsigned long long factorialIterative(int n)
{
	long long fact = 1;
	for (int i = 1; i <= n; ++i)
		fact *= i;
	return fact;
}

unsigned long long factorialRecursive(int n)
{
	if (n < 2)
		return 1;
	return n*factorialRecursive(n - 1);
}

int fibCounter = 0;
unsigned long long fibonacciIterative(int n)
{
	unsigned long long sum = 1;
	unsigned long long prevSum = 1;
	unsigned long long prevPrevSum = 0;

	for (int i = 1; i < n; ++i)
	{
		sum = prevSum + prevPrevSum;
		prevPrevSum = prevSum;
		prevSum = sum;		
	}

	return sum;
}

int sum(int N, int x, ...)
{
	va_list args;
	va_start(args, x);
	int result = x;
	for (int i = 1; i < N; ++i)
		result += va_arg(args, int);
	va_end(args);
	return result;
}

int prod(int x, ...)
{
	va_list args;
	va_start(args, x);
	int result = 1;
	while (x != 0)
	{
		result *= x;
		x = va_arg(args, int);
	}
	va_end(args);
	return result;
}

unsigned long long fibonacciRecursive(int n)
{
	++fibCounter;
	if (n < 3)
		return 1;
	return fibonacciRecursive(n - 1) + fibonacciRecursive(n - 2);
}

int main()
{	
	long ms;
	int n = 20;

	tic();
	long long fIter = factorialIterative(n);
	ms = toc();

	cout << "Iterative " << n << "! = " << fIter << endl;
	cout << "Iterative time: " << ms << endl;

	tic();
	long long fRec = factorialRecursive(n);
	ms = toc();

	cout << "Recursive " << n << "! = " << fRec << endl;
	cout << "Recursive time: " << ms << endl;

	n = 20;
	tic();
	long long fibIter = fibonacciIterative(n);
	ms = toc();
	cout << "Fibonacci iterative: " << n << "-th elem: " << fibIter << endl;
	cout << "Fibonacci iterative time: " << ms << endl;

	tic();
	long long fibRec = fibonacciRecursive(n);
	ms = toc();
	cout << "Fibonacci recursive: " << n << "-th elem: " << fibRec << endl;
	cout << "Fibonacci recursive time: " << ms << endl;
	cout << "Fibonacci recursive counter: " << fibCounter << endl;

	cout << "Varargs sum: " << sum(4, 1, 0, 2, 3) << endl;
	cout << "Varargs prod: " << prod(1,2,4,0) << endl;

	system("pause");
	return 0;
}