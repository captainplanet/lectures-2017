#include <iostream>
using namespace std;

int main()
{
	//1D array
	const int size = 3;

	int intArray[size] = { 1,2,3 }; //int* const intArray;
	intArray[1] = 5;

	cout << "Size of array: " << intArray << endl;
	cout << endl;

	for (int i = 0; i < 3; ++i)
		cout << intArray[i] << endl; //*(intArray+i)

	//2D array
	const int rows = 2;
	const int cols = 3;
	int intArray2D[rows][cols] = { {1,2,3},{4,5,6} };
	intArray2D[0][1] = 10;
	cout << endl;
	for (int i = 0; i < rows; ++i)
	{
		for (int j = 0; j < cols; ++j)
		{
			cout << intArray2D[i][j] << "\t";
		}
		cout << endl;
	}

	for (int i = 0; i < 100; ++i)
	{
		int* x = new int[1024 * 1024 * 1000];
		for (int j = 0; j < 1e12; ++j);
		cout << "yay" << endl;
	}

	system("pause");
	return 0;
}