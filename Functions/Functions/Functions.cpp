#include "Functions.h"
#include <iostream>

int sum(int a, int b)
{
	a = 5;
	return a + b;
}

void printHello()
{
	std::cout << "Hello" << std::endl;
}

void swapp(int* a, int* b)
{
	int tmp = *a;
	*a = *b;
	*b = tmp;
}

void swapp(int& a, int& b)
{
	int tmp = a;
	a = b;
	b = tmp;
}

double power(double val, int degree)
{
	double result = val;
	for (int i = 1; i < degree; ++i)
		result *= val;
	return result;
}

//1D maximum
int max(int arr[], int size)
{
	int tmp = arr[0];
	for (int i = 0; i < size; ++i)
		if (arr[i] > tmp)
			tmp = arr[i];
	return tmp;
}

//2D maximum
int& max(int** arr, int rows, int cols)
{
	int tmp = arr[0][0];
	for (int i = 0; i < rows; ++i)
		for (int j = 0; j < cols; ++j)
			if (arr[i][j] > tmp)
				tmp = arr[i][j];
	return tmp;
}

//random array
int* randArray(int size)
{
	int* tmp = new int[size];
	for (int i = 0; i < size; ++i)
		tmp[i] = rand() % 21 - 10;
	return tmp;
}

void printArray(int * arr, int size)
{
	for (int i = 0; i < size; ++i)
		std::cout << arr[i] << '\t';
	std::cout << std::endl;
}

int countInvokes()
{
	static int count = 0;
	++count;
	return count;
}