#ifndef FUNCTIONS_H
#define FUNCTIONS_H

int sum(int, int);
void printHello();
void swapp(int*, int*);
void swapp(int&, int&);
double power(double, int = 2);
int max(int arr[], int size);
int& max(int** arr2d, int rows, int cols);
int countInvokes();
//int max(int* arr, int size);

int* randArray(int size);
void printArray(int* arr, int size);

//int arr[]; //int * const arr;


#endif
