#include <iostream>
#include "Functions.h"
#include <time.h>
using namespace std;

int main()
{
	srand(time(NULL));
	int x = 1;
	int y = 2;
	cout << sum(x, y) << endl;
	printHello();
	cout << x << endl;
	swapp(&x, &y);
	cout << x << " " << y << endl;
	swapp(x, y);
	cout << x << " " << y << endl;

	cout << power(4, 3) << endl;
	cout << power(4) << endl;
	cout << "Array functions: " << endl;
	int arr[3] = { 1,2,3 };
	
	cout << "Maximum: " << max(arr,3) << endl;
	cout << arr[1] << endl;

	int** arr2d = new int*[2];
	for (int i = 0; i < 2; ++i)
	{
		arr2d[i] = new int[2];
		for (int j = 0; j < 2; ++j)
			arr2d[i][j] = i*2 + j;
	}
	// **arr2d
	int& max2d = max((int**)arr2d, 2, 2);
	cout << "Maximum 2D: " << max2d << endl;
	max2d = 10;
	cout << "Maximum 2D: " << arr2d[1][1] << endl;

	int* randomArray = randArray(5);
	int* tmpP = randomArray;
	cout << tmpP << " " << randomArray << endl;
	printArray(randomArray,5);
	delete[] randomArray;
	cout << tmpP << " " << randomArray << endl;

	for (int i = 0; i < 3; ++i)
		cout << countInvokes() << endl;
	
	system("pause");
	return 0;
}