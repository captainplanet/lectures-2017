#include <iostream>
using namespace std;

int main()
{
	int x = 5;
	int x2 = 10;
	int* p = &x;
	int* p2 = &x2;
	int pdiff = p - p2;

	cout << "pdiff: " << pdiff << endl;

	double dx = 1.0;
	double* pd = &dx;

	cout << "Adress of x: " << p << endl;
	cout << "Adress of x2: " << &x2 << endl;
	cout << "Sizeof int pointer: " << sizeof(p) << endl;

	cout << "Value of variable with address in p: " << endl;
	cout << *p << endl;
	*p = 10;
	cout << x << endl;


	cout << "x2 through p: " << *(p-pdiff) << endl;

	cout << pd << endl;
	cout << sizeof(pd) << endl;

	system("pause");
	return 0;
}