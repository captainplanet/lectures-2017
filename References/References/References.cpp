#include <iostream>
#include <time.h>
using namespace std;

int main()
{
	srand(time(NULL));
	const int size = 10;
	int* randArray = new int[size];
	for (int i = 0; i < size; ++i)
	{
		randArray[i] = rand() % 21 - 10;
		cout << randArray[i] << "\t";
	}
	cout << endl;

	int& val1 = randArray[rand() % size];
	int& val2 = randArray[rand() % size];
	int tmp = val2;
	val2 = val1;
	val1 = tmp;

	for (int i = 0; i < size; ++i)
	{
		cout << randArray[i] << "\t";
	}
	cout << endl;

	delete[] randArray;

	cout << val1 << " " << val2 << endl;
	val1 = 5;
	cout << val1 << " " << val2 << endl;

	system("pause");
	return 0;
}