#include <iostream>
#include <fstream>
using namespace std;

int main()
{
	fstream out;
	out.open("out.txt", ios::out);
	if (!out.is_open())
	{
		cout << "File error" << endl;
		system("pause");
		return -1;
	} 
	out << "String11" << endl;
	out.put('\r');
	out.put('\0');
	out.close();

	ifstream fin;
	fin.open("out.txt");
	if (!fin.is_open())
	{
		cout << "File error" << endl;
		system("pause");
		return -1;
	}
	while (!fin.eof())
	{
		cout << fin.get() << endl;
	}

	fin.close();
	system("pause");
	return 0;
}