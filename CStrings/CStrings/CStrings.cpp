#include <iostream>
using namespace std;

int main()
{
	//
	char str[64] = "Hello world!";
	str[4] = ' ';
	cout << str << endl;
	cout << "Last symbol: " << (int)str[12] << endl;

	char str1[6] = { 'H','e','l','l','o',' ' }; //="Hello";
	str1[6 + 10] = 'h';
	cout << str1 << endl;
	cout << str << endl;

	char strConcat[64] = "Goodbye world!";
	cout << strlen(strConcat) << endl;
	cout << strlen(str1) << endl;
	strcat_s(str,strConcat);
	cout << str << endl;
	strcpy_s(str, strConcat);
	cout << str << endl;
	cout << "-----------------" << endl;
	char strFromConsole[64];
	cin >> strFromConsole;
	cout << strFromConsole << endl;
	cin.getline(strFromConsole, 10);
	char c = cin.get();
	cout << strFromConsole << endl << c;


	system("pause");
	return 0;
}