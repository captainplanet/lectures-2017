#include <iostream>
#include "VectorInt.h"
using namespace std;

struct VectorStruct
{
	int* data;
	int size;
};

int main()
{
	VectorStruct vec;
	vec.data = new int[10];
	vec.size = 11;
	for (int i = 0; i < vec.size; ++i)
		vec.data[i] = i;

	VectorInt vecInt(5);
	VectorInt vecCopy = vecInt;
	vecInt.elementAt(0) = 2;
	cout << "vecInt[0] = " << vecInt.elementAt(0) << endl;
	vecInt.pushBack(10).pushBack(20).pushBack(30).getSize();
	cout << "Object count: " << vecInt.getObjectCount() << endl;
	cout << "Object count: " << vecCopy.getObjectCount() << endl;;
	cout << "Object count: " << VectorInt::getObjectCount() << endl;

	system("pause");
	return 0;
}