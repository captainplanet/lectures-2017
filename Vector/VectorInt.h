#include <iostream>
using namespace std;

class VectorInt
{
public:
	//����������� �� ���������
	VectorInt(){ objectCount++; }
	//���������� �����������
	VectorInt(const VectorInt& vec);
	VectorInt(int _size);
	VectorInt(int* _data, int _size);

	//����������
	~VectorInt();

	int getSize() const;
	void setSize(int _size);
	int& elementAt(int index) const;
	VectorInt& pushBack(int value);

	void print() const;
	
	const int defSize = 5;
	static int getObjectCount();

	//������� ����� ������ � private-����� � �������
	friend void friendFunc(const VectorInt& v);
	friend class FriendClass;
private:
	int* data;
	int size;
	static int objectCount;
protected:
	int testVar;
};

class FriendClass
{
public:
	void TestMethod(VectorInt& v)
	{
		v.size = 5;
		v.testVar = 1;
	}
};