#include "VectorInt.h"

//Копирующий конструктор
VectorInt::VectorInt(const VectorInt& vec)
{
	objectCount++;
	size = vec.size;
	data = new int[size];
	for (int i = 0; i < size; ++i)
		data[i] = vec.data[i];
}

VectorInt::VectorInt(int _size)
{
	objectCount++;
	size = _size;
	data = new int[size];
}

VectorInt::VectorInt(int* _data, int _size)
{
	objectCount++;
	size = _size;
	data = new int[size];
	for (int i = 0; i < size; ++i)
		data[i] = _data[i];
}

VectorInt::~VectorInt()
{
	objectCount--;
	if (size > 0)
		delete[] data;
}

int VectorInt::getSize() const
{
	return size;
}

void VectorInt::setSize(int _size)
{
	if (size > 0)
		delete[] data;

	size = _size;
	data = new int[size];
}

int& VectorInt::elementAt(int index) const
{
	if (index < 0 || index > size - 1)
		throw out_of_range("Index out of bounds");
	return data[index];
}

VectorInt& VectorInt::pushBack(int value)
{
	if (size > 0)
	{
		int* tmp = new int[size];
		for (int i = 0; i < size; ++i)
			tmp[i] = data[i];
		delete[] data;
	}
	++size;
	data = new int[size];
	data[size - 1] = value;
	return *this;
}

void VectorInt::print() const
{
	for (int i = 0; i < size; ++i)
		cout << data[i] << endl;
}

int VectorInt::getObjectCount()
{
	return objectCount;
}

int VectorInt::objectCount = 0;

void friendFunc(const VectorInt & v)
{
	cout << v.size << endl;
}
