#include <iostream>
using namespace std;

int main() 
{
	const int rows = 5;
	const int cols = 5;
	int array[rows][cols];
	int count = 0;
	for (int i = 0; i < rows; ++i)
	{
		bool even = (i % 2 == 0);
		int j = even ? 0 : (cols - 1);
		int increment = even ? 1 : -1;
		for (j; (j >= 0) && (j < cols); j += increment)
			array[i][j] = ++count;
	}

	for (int i = 0; i < rows; ++i)
	{
		for (int j = 0; j < cols; ++j)
			cout << array[i][j] << '\t';
		cout << endl;
	}
	cout << endl;
	system("pause");
	return 0;
}