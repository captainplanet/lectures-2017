#pragma once
#include <iostream>
#include <algorithm>
#include "Tools.h"
using namespace std;

#define DBG_TIME
//#define DBG_OPERATIONS

template <typename Type>
void bubbleSort(Type* array, int size, bool(*cmp)(Type,Type))
{
	#ifdef DBG_TIME
		tic();
	#endif

	#ifdef DBG_OPERATIONS
		int cmpCount = 0;
		int swapCount = 0;
	#endif

	for (int i = 0; i<size - 1; ++i)
		for (int j = 0; j < size - i - 1; ++j)
		{
			#ifdef DBG_OPERATIONS
				cmpCount++;
			#endif

			if (cmp(array[j], array[j + 1]))
			{
				#ifdef DBG_OPERATIONS
					swapCount++;
				#endif

				swap(array[j], array[j + 1]);
			}
		}

	#ifdef DBG_TIME
		long ms = toc();
		cout << "BubbleSort time: " << ms << endl;
	#endif

	#ifdef DBG_OPERATIONS
		cout << "BubbleSort comparisons: " << cmpCount << endl;
		cout << "BubbleSort swaps: " << swapCount << endl;
		cout << endl;
	#endif

}

template <typename Type>
void selectionSort(Type* array, int size, bool(*cmp)(Type, Type))
{
	#ifdef DBG_TIME
		tic();
	#endif

	#ifdef DBG_OPERATIONS
		int cmpCount = 0;
		int swapCount = 0;
	#endif

	for (int i = 0; i < size - 1; ++i)
	{
		int idx = 0;
		for (int j = 1; j < size - i; ++j)
		{
			#ifdef DBG_OPERATIONS
				++cmpCount;
			#endif
			if (cmp(array[j], array[idx]))
				idx = j;
		}

		#ifdef DBG_OPERATIONS
				++swapCount;
		#endif
		swap(array[idx], array[size - i - 1]);
	}

	#ifdef DBG_TIME
		long ms = toc();
		cout << "SelectionSort time: " << ms << endl;
	#endif

	#ifdef DBG_OPERATIONS
		cout << "SelectionSort comparisons: " << cmpCount << endl;
		cout << "SelectionSort swaps: " << swapCount << endl;
		cout << endl;
	#endif
}

template <typename Type>
void insertionSort(Type* array, int size, bool(*cmp)(Type, Type))
{
	#ifdef DBG_TIME
		tic();
	#endif

	#ifdef DBG_OPERATIONS
		int cmpCount = 0;
		int swapCount = 0;
	#endif

	for (int i = size - 2; i >= 0; --i)
	{
		Type key = array[i];
		for (int j = i + 1; j < size; ++j)
		{
			#ifdef DBG_OPERATIONS
				++cmpCount;
			#endif
			if (cmp(key, array[j]))
			{
				#ifdef DBG_OPERATIONS
					++swapCount;
				#endif
				array[j - 1] = array[j];
			}
			else
			{
				#ifdef DBG_OPERATIONS
					++swapCount;
				#endif
				array[j - 1] = key;
				break;
			}
		}
	}

	#ifdef DBG_TIME
		long ms = toc();
		cout << "insertionSort time: " << ms << endl;
	#endif

	#ifdef DBG_OPERATIONS
		cout << "insertionSort comparisons: " << cmpCount << endl;
		cout << "insertionSort swaps: " << swapCount << endl;
		cout << endl;
	#endif
}

template<typename Type>
int binarySearch(Type* array, int size, Type value,bool(*cmp)(Type, Type))
{
	int leftBound = 0;
	int rightBound = size - 1;

	while (leftBound < rightBound)
	{
		int currIdx = leftBound + (rightBound - leftBound) / 2;
		if (cmp(array[currIdx], value))
			rightBound = currIdx - 1;
		else
			leftBound = currIdx + 1;
	}

	return leftBound;
}

template <typename Type>
void insertionSortBinary(Type* array, int size, bool(*cmp)(Type, Type))
{
#ifdef DBG_TIME
	tic();
#endif

#ifdef DBG_OPERATIONS
	int cmpCount = 0;
	int swapCount = 0;
#endif

	for (int i = size - 2; i >= 0; --i)
	{
		Type key = array[i];
		int idx = binarySearch<Type>(array + i + 1, size - i - 1,key, cmp) + i + 1;
		int rightBound = cmp(key, array[idx]) ? idx : idx - 1;

		for (int j = i + 1; j <= rightBound; ++j)
		{
			array[j - 1] = array[j];
		}
		array[rightBound] = key;
	}

#ifdef DBG_TIME
	long ms = toc();
	cout << "insertionSortBin time: " << ms << endl;
#endif

#ifdef DBG_OPERATIONS
	cout << "insertionSortBin comparisons: " << cmpCount << endl;
	cout << "insertionSortBin swaps: " << swapCount << endl;
	cout << endl;
#endif
}

template <typename Type>
void mergeSortRecursive(Type* array, int size, bool(*cmp)(Type, Type))
{
#ifdef DBG_TIME
	tic();
#endif

	mergeSort(array, size, cmp);

#ifdef DBG_TIME
	long ms = toc();
	cout << "mergeSortRecursive time: " << ms << endl;
#endif
}

template <typename Type>
void mergeSort(Type* array, int size, bool(*cmp)(Type, Type))
{
	if (size > 1)
	{
		int splitIdx = size / 2;
		mergeSort(array, splitIdx, cmp);
		mergeSort(array + splitIdx, size - splitIdx, cmp);
		merge(array, size, splitIdx, cmp);
	}
}

template <typename Type>
void merge(Type* array, int size, int splitIdx, bool(*cmp)(Type, Type))
{
	int lPos = 0;
	int rPos = splitIdx;
	int pos = 0;
	
	Type* tmpSorted = new Type[size];
	while (lPos < splitIdx && rPos < size)
	{
		if (!cmp(array[lPos], array[rPos]))
			tmpSorted[pos++] = array[lPos++];
		else
			tmpSorted[pos++] = array[rPos++];

		//������������� ���������
		//tmpSorted[pos++] = cmp(array[lPos], array[rPos]) ? array[lPos++] : array[rPos++];
	}

	while (lPos < splitIdx)
		tmpSorted[pos++] = array[lPos++];

	while (rPos < size)
		tmpSorted[pos++] = array[rPos++];

	for (int i = 0; i < size; ++i)
		array[i] = tmpSorted[i];

	delete[] tmpSorted;
}

template <typename Type>
void mergeSortIterative(Type* array, int size, bool(*cmp)(Type, Type))
{
#ifdef DBG_TIME
	tic();
#endif
	int blockSize = 1;
	int blockIdx = 0;
	for (; blockSize < size; blockSize *= 2)
	{
		blockIdx = 0;
		for (; blockIdx < size - blockSize; blockIdx += 2 * blockSize)
		{
			merge(array+blockIdx,
				min(blockSize*2,size-blockIdx),
				blockSize,
				cmp);
		}
	}

#ifdef DBG_TIME
	long ms = toc();
	cout << "mergeSortIterative time: " << ms << endl;
#endif
}

template <typename Type>
void heapElem(Type* array, int size, int idx, bool(*cmp)(Type, Type))
{
	while (idx * 2 + 1 < size)
	{
		if (idx * 2 + 2 < size)
		{
			int maxIdx = cmp(array[idx * 2 + 1], array[idx * 2 + 2]) ? idx * 2 + 1 : idx * 2 + 2;
			if (cmp(array[maxIdx], array[idx]))
			{
				swap(array[maxIdx], array[idx]);
				idx = maxIdx;
			}
			else
				break;
		}
		else
		{
			if (cmp(array[2 * idx + 1], array[idx]))
			{
				swap(array[2 * idx + 1], array[idx]);
			}
			break;
		}
	}
}

template <typename Type>
void heapSort(Type* array, int size, bool(*cmp)(Type, Type))
{
#ifdef DBG_TIME
	tic();
#endif
	//1. Make heap
	for (int i = size / 2 - 1; i >= 0; --i)
		heapElem(array, size, i, cmp);
	//2. Sort
	for (int i = 0; i < size; ++i)
	{
		swap(array[0], array[size - i - 1]);
		heapElem(array, size - i - 1, 0, cmp);
	}

#ifdef DBG_TIME
	long ms = toc();
	cout << "heapSort time: " << ms << endl;
#endif
}

template <typename Type>
void qSortRec(Type* array, int size, bool(*cmp)(Type, Type))
{
	if (size == 2)
	{
		if (cmp(array[0], array[1]))
			swap(array[0], array[1]);
		return;
	}
	if (size <= 1)
		return;

	int keyIdx = size / 2;
	Type key = array[keyIdx];
	int lPos = 0;
	int rPos = size - 1;

	while (lPos < rPos)
	{
		while (cmp(key, array[lPos]) && lPos < keyIdx)
			++lPos;
		while (cmp(array[rPos], key) && rPos > keyIdx)
			--rPos;
		if (lPos == rPos)
			break;

		swap(array[lPos], array[rPos]);

		if (lPos == keyIdx)
			keyIdx = rPos;

		else if (rPos == keyIdx)
			keyIdx = lPos;
	}
	qSortRec(array, keyIdx,cmp);
	qSortRec(array + keyIdx + 1, size - keyIdx - 1, cmp);
}

template <typename Type>
void qSort(Type* array, int size, bool(*cmp)(Type, Type))
{
#ifdef DBG_TIME
	tic();
#endif

	qSortRec(array, size, cmp);

#ifdef DBG_TIME
	long ms = toc();
	cout << "qSort time: " << ms << endl;
#endif
}

void radixSortRec(int* array, int size, int digit);

void radixSort(int* array, int size)
{
#ifdef DBG_TIME
	tic();
#endif
	for (int i = 0; i < size; ++i)
		array[i] ^= INT_MIN;

	radixSortRec(array, size, 31);

	for (int i = 0; i < size; ++i)
		array[i] ^= INT_MIN;

#ifdef DBG_TIME
	long ms = toc();
	cout << "qSort time: " << ms << endl;
#endif
}

void radixSortRec(int* array, int size, int digit)
{
	if (size <= 1)
		return;

	if (digit < 0)
		return;

	int leftIdx = 0;
	int rightIdx = size - 1;
	int mask = 1 << digit;

	while (true)
	{
		while ((leftIdx < rightIdx) && ((array[leftIdx] & mask) == 0))
			++leftIdx;
		while ((rightIdx > leftIdx) && ((array[rightIdx] & mask) != 0))
			--rightIdx;
		if (leftIdx != rightIdx)
			swap(array[leftIdx], array[rightIdx]);
		else
			break;
	}
	//printArray<int>(array, size, [](int x) {cout << x << " "; });
	//cout << endl;
	int splitIdx = ((array[leftIdx] & mask) == 0) ? leftIdx + 1 : leftIdx;
	radixSortRec(array, splitIdx, digit - 1);
	radixSortRec(array + splitIdx, size - splitIdx, digit - 1);
}