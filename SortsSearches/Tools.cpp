#include "Tools.h"
#include <math.h>
#include <chrono>
using namespace std::chrono;

high_resolution_clock::time_point sstart;
void tic()
{
	sstart = high_resolution_clock::now();
}

long toc()
{
	return duration_cast<milliseconds>(high_resolution_clock::now() - sstart).count();
}

double abs(Point p)
{
	return sqrt(p.x*p.x + p.y*p.y);
}

bool pointGreater(Point a, Point b)
{
	return abs(a) > abs(b);
}

bool intGreater(int a, int b)
{
	return a > b;
}

bool intGreaterOrEqual(int a, int b)
{
	return a >= b;
}

bool doubleGreater(double a, double b)
{
	return a > b;
}

bool doubleGreaterOrEqual(double a, double b)
{
	return a >= b;
}
