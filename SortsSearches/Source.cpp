#include <iostream>
#include <time.h>
#include "Sorts.h"
#include "Tools.h"
using namespace std;

int main()
{
	srand(time(NULL));
	int size = 1e5;
	int* array = new int[size];
	int* randomArray = new int[size];
	int* almostSortedArray = new int[size];
	int* unsortedArray = new int[size];

	auto printRowElem = [](int x) {cout << x << " "; };
	auto copyXtoY = [](int& x, int&y) {y = x; };

	fillAlmostSortedArray(almostSortedArray, size);
	fillUnsortedArray(unsortedArray, size);
	fillRandomArray<int>(randomArray, size, [=](int& x) {x = rand() - RAND_MAX/2; });
	
	//cout << "BubbleSort almost sorted: " << endl;
	//copyArray<int>(almostSortedArray, array, size, copyXtoY);
	//bubbleSort<int>(array, size, intGreater);

	//cout << "BubbleSort random: " << endl;
	//copyArray<int>(randomArray, array, size, copyXtoY);
	//bubbleSort<int>(array, size, intGreater);
	//
	//cout << "BubbleSort unsorted: " << endl;
	//copyArray<int>(unsortedArray, array, size, copyXtoY);
	//bubbleSort<int>(array, size, intGreater);

	//cout << "SelectionSort almost sorted: " << endl;
	//copyArray<int>(almostSortedArray, array, size, copyXtoY);
	//selectionSort<int>(array, size, intGreater);

	//cout << "SelectionSort random: " << endl;
	//copyArray<int>(randomArray, array, size, copyXtoY);
	//selectionSort<int>(array, size, intGreater);

	//cout << "SelectionSort unsorted: " << endl;
	//copyArray<int>(unsortedArray, array, size, copyXtoY);
	//selectionSort<int>(array, size, intGreater);

	//cout << "insertionSort almost sorted: " << endl;
	//copyArray<int>(almostSortedArray, array, size, copyXtoY);
	//insertionSort<int>(array, size, intGreater);

	//cout << "insertionSort random: " << endl;
	//copyArray<int>(randomArray, array, size, copyXtoY);
	//insertionSort<int>(array, size, intGreater);

	//cout << "insertionSort unsorted: " << endl;
	//copyArray<int>(unsortedArray, array, size, copyXtoY);
	//insertionSort<int>(array, size, intGreater);

	//cout << "insertionSortBin almost sorted: " << endl;
	//copyArray<int>(almostSortedArray, array, size, copyXtoY);
	//insertionSortBinary<int>(array, size, intGreater);

	//cout << "insertionSortBin random: " << endl;
	//copyArray<int>(randomArray, array, size, copyXtoY);
	//insertionSortBinary<int>(array, size, intGreater);

	//cout << "insertionSortBin unsorted: " << endl;
	//copyArray<int>(unsortedArray, array, size, copyXtoY);
	//insertionSortBinary<int>(array, size, intGreater);

	cout << "mergeSortRecursive almostSorted: " << endl;
	copyArray<int>(almostSortedArray, array, size, copyXtoY);
	mergeSortRecursive<int>(array, size, intGreater);

	cout << "mergeSortRecursive random: " << endl;
	copyArray<int>(randomArray, array, size, copyXtoY);
	mergeSortRecursive<int>(array, size, intGreater);

	cout << "mergeSortRecursive unsorted: " << endl;
	copyArray<int>(unsortedArray, array, size, copyXtoY);
	mergeSortRecursive<int>(array, size, intGreater);

	//cout << "mergeSortIterative almostSorted: " << endl;
	//copyArray<int>(almostSortedArray, array, size, copyXtoY);
	//mergeSortIterative<int>(array, size, intGreater);

	//cout << "mergeSortIterative random: " << endl;
	//copyArray<int>(randomArray, array, size, copyXtoY);
	//mergeSortIterative<int>(array, size, intGreater);

	//cout << "mergeSortIterative unsorted: " << endl;
	//copyArray<int>(unsortedArray, array, size, copyXtoY);
	//mergeSortIterative<int>(array, size, intGreater);

	cout << "heapSort almostSorted: " << endl;
	copyArray<int>(almostSortedArray, array, size, copyXtoY);
	heapSort<int>(array, size, intGreater);

	cout << "heapSort random: " << endl;
	copyArray<int>(randomArray, array, size, copyXtoY);
	heapSort<int>(array, size, intGreater);

	cout << "heapSort unsorted: " << endl;
	copyArray<int>(unsortedArray, array, size, copyXtoY);
	heapSort<int>(array, size, intGreater);

	cout << "qSort almostSorted: " << endl;
	copyArray<int>(almostSortedArray, array, size, copyXtoY);
	qSort<int>(array, size, intGreaterOrEqual);

	cout << "qSort random: " << endl;
	copyArray<int>(randomArray, array, size, copyXtoY);
	qSort<int>(array, size, intGreaterOrEqual);

	cout << "qSort unsorted: " << endl;
	copyArray<int>(unsortedArray, array, size, copyXtoY);
	qSort<int>(array, size, intGreaterOrEqual);

	cout << "radix almostSorted: " << endl;
	copyArray<int>(almostSortedArray, array, size, copyXtoY);
	radixSort(array, size);

	cout << "radix random: " << endl;
	copyArray<int>(randomArray, array, size, copyXtoY);
	radixSort(array, size);

	cout << "radix unsorted: " << endl;
	copyArray<int>(unsortedArray, array, size, copyXtoY);
	radixSort(array, size);

	delete[] array;
	delete[] randomArray;
	delete[] almostSortedArray;
	delete[] unsortedArray;

	system("pause");
	return 0;
}