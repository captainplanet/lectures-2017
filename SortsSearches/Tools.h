#pragma once
#include <iostream>
#include <functional>
using namespace std;

struct Point
{
	int x;
	int y;
};

double abs(Point p);
bool pointGreater(Point a, Point b);

bool intGreater(int a, int b);
bool intGreaterOrEqual(int a, int b);
bool doubleGreater(double a, double b);
bool doubleGreaterOrEqual(double a, double b);

void tic();
long toc();

template<typename Type>
void printArray(Type* array, int size, function<void(Type)> print)
{
	for (int i = 0; i < size; ++i)
		print(array[i]);
	cout << endl;
}

template<typename Type>
void fillRandomArray(Type* array, int size, function<void(Type&)> randObj)
{
	for (int i = 0; i < size; ++i)
		randObj(array[i]);
}

template<typename Type>
void fillAlmostSortedArray(Type* array, int size)
{
	double delta = (double) 1 / RAND_MAX;
	
	for (int i = 0; i < size; ++i)
		array[i] = delta*i;

	swap(array[size - 1], array[size - 2]);
}

template<typename Type>
void fillUnsortedArray(Type* array, int size)
{
	double delta = (double) 1 / RAND_MAX;

	for (int i = size-1; i >=0 ; --i)
		array[size - i - 1] = delta*i;
}

template<typename Type>
void copyArray(Type* sourceArray, 
				Type* destArray,
				int size, 
				function<void(Type&,Type&)> copyObj)
{
	for (int i = 0; i < size; ++i)
		copyObj(sourceArray[i], destArray[i]);
}