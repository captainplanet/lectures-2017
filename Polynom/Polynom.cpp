#include "Polynom.h"
#include <exception>
#include <iostream>
using namespace std;

Polynom::Polynom()
{
	size = 1;
	coeffs = new double[1];
}

Polynom::Polynom(double * _coeffs, int _size)
{
	if (_size < 1)
		throw std::exception("Wrong size");

	size = _size;
	coeffs = new double[size];
	for (int i = 0; i < size; ++i)
		coeffs[i] = _coeffs[i];
}

Polynom::Polynom(const Polynom& poly)
{
	size = poly.size;
	coeffs = new double[size];
	for (int i = 0; i < size; ++i)
		coeffs[i] = poly.coeffs[i];
}

Polynom::~Polynom()
{
	delete[] coeffs;
}

double& Polynom::operator[](int index)
{
	if (index < 0 || index > size - 1)
		throw std::exception("Out of bounds");

	return coeffs[index];
}

double Polynom::operator()(double x)
{
	double currX = 1;
	double polyVal = coeffs[0];
	for (int i = 1; i < size; ++i)
		polyVal += coeffs[i]*currX*x;

	return polyVal;
}

Polynom& Polynom::operator=(const Polynom& p)
{
	delete[] coeffs;

	size = p.size;
	coeffs = new double[size];
	for (int i = 0; i < size; ++i)
		coeffs[i] = p.coeffs[i];
	return *this;
}

ostream& operator<<(ostream& out, const Polynom& poly)
{
	out << poly.coeffs[0];
	for (int i = 1; i < poly.size; ++i)
		out << " + " << poly.coeffs[i] << "x^" << i;

	return out;
}

Polynom& operator+(const Polynom& a, const Polynom& b)
{
	
	if (a.size >= b.size)
	{
		Polynom* sum = new Polynom(a);

		for (int i = 0; i < b.size; ++i)
			sum->coeffs[i] += b.coeffs[i];
		return *sum;
	}
	else
	{
		Polynom* sum = new Polynom(b);
		for (int i = 0; i < a.size; ++i)
			sum->coeffs[i] += a.coeffs[i];
		return *sum;
	}
}
