#include <iostream>
using namespace std;

class Polynom
{
public:
	Polynom();
	Polynom(double* _coeffs, int _size);
	Polynom(const Polynom&);
	~Polynom();

	int getDegree() { return size - 1; }

	double& operator[](int index);
	double operator()(double x);
	friend ostream& operator<<(ostream& out, const Polynom& poly);
	friend Polynom& operator+(const Polynom&, const Polynom&);
	Polynom& operator=(const Polynom&);

private:
	double* coeffs;
	int size;
};