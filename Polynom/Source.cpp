#include <iostream>
#include "Polynom.h"
using namespace std;

int main()
{
	double coeffs[3] = { 1, 2, 1 };
	Polynom poly(coeffs, 3);
	Polynom poly2(coeffs, 3);
	cout << poly << endl;
	cout << poly(2) << endl;

	Polynom polySum = poly + poly2;
	cout << polySum << endl;


	system("pause");
	return 0;
}