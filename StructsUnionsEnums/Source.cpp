#include <iostream>
#include <time.h>
#include <fstream>
using namespace std;

union Useless
{
	short z;
	int x;
	short y;
};

// 0 -- Monday
// 1 -- Tuesday
//void calendarTask(int day);
enum DayOfWeek
{
	Monday=1,
	Tuesday,
	Wednesday,
	Thursday,
	Friday,
	Saturday,
	Sunday
};

void RobertSmithTextGenerator(DayOfWeek day)
{
	switch (day)
	{
		case Monday:
		{
			cout << "Monday, you can fall apart" << endl;
			break;
		}
		case Tuesday:
		{
			cout << "Tuesday, break my heart" << endl;
			break;
		}
		case Wednesday:
		{
			cout << "Wednesday, break my heart" << endl;
			break;
		}
		case Thursday:
		{
			cout << "Thursday, doesn't event start" << endl;
			break;
		}
		case Friday:
		{
			cout << "Friday, I'm in love" << endl;
			break;
		}
		case Saturday:
		{
			cout << "Saturday, wait" << endl;
			break;
		}
		case Sunday:
		{
			cout << "Sunday, always comes too late" << endl;
			break;
		}
	}
}

struct TestStruct
{
	int x;
	int z;
	double y;
};

#pragma pack(push,1)
struct StructWithoutPadding
{
	int x;
	double y;
	int z;
	short a;
};
#pragma pack(pop)

struct BitFields
{
	char x : 2;
	char y : 3;
	char z : 3;
};

struct Line2D
{
	double A;
	double B;
	double C;
};

struct Point
{
	double x;
	double y;
};

Point lineCrossing(Line2D line1, Line2D line2)
{
	Point crossing;
	double det = line1.A*line2.B - line2.A*line1.B;
	double det1 = -line1.C*line2.B + line2.C*line1.B;
	double det2 = -line2.C*line1.A + line1.C*line2.A;

	if (det == 0)
	{
		if (det1 == 0)
			throw "Lines are equal";
		else
			throw "Lines are parallel";
	}

	crossing.x = det1 / det;
	crossing.y = det2 / det;

	return crossing;
}

int main()
{
#pragma region UNIONS
	//UNIONS
	cout << sizeof(Useless) << endl;
	Useless var;
	var.x = 17;
	var.z = 10;
	
	cout << var.y << endl;
#pragma endregion

#pragma region ENUM
	srand(time(NULL));
	//DayOfWeek day = Monday;
	//DayOfWeek day = DayOfWeek(Tuesday);
	DayOfWeek day = DayOfWeek(2);
	cout << (day == Tuesday) << endl;
	cout << (day == Thursday) << endl;
	for (int i = 0; i < 10; ++i)
		RobertSmithTextGenerator(DayOfWeek(rand() % 7 + 1));

#pragma endregion

#pragma region STRUCTS
	cout << endl << "STRUCTS" << endl << endl;
	cout << sizeof(TestStruct) << endl;
	cout << "Without padding: " << sizeof(StructWithoutPadding) << endl;
	cout << "Bit fields: " << sizeof(BitFields) << endl;
	BitFields bf;
	bf.x = 6;
	cout << (int)bf.x << endl;
	cout << (int)bf.y << endl;

	Line2D line1{ 1, -1, 0};
	Line2D line2{ 1, 1, -2};
	try
	{
		Point cross = lineCrossing(line1, line2);
		cout << cross.x << " " << cross.y << endl;
	}
	catch (char* message)
	{
		cout << "Exception thrown: " << message << endl;
	}
	
#pragma endregion

#pragma region StructToFile

	Line2D lineToFile{ 2, 1.5, -8 };
	Line2D lineFromFile;
	fstream file;

	file.open("line.out", ios::binary | ios::out);
	//TODO: check if file is actually open
	file.write((char*)&lineToFile, sizeof(Line2D));
	file.close();

	file.open("line.out", ios::binary | ios::in);
	//TODO: check if file is actually open
	file.read((char*)&lineFromFile, sizeof(Line2D));
	file.close();

	cout << "Line from file: " << lineFromFile.A << " ";
	cout <<  lineFromFile.B << " " << lineFromFile.C << endl;
#pragma endregion



	system("pause");
	return 0;
}