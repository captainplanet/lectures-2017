#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSerialPort>
#include <QDebug>
#include <QTcpSocket>
#include <QHostAddress>
#include <QDateTime>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QSerialPort* port;
    QTcpSocket* socket;

    void sendMessage();

private slots:
    void on_pushButtonSocketConnect_clicked();
    void on_pushButtonSendMessage_clicked();
    void connectClick();
    void sendClick();
    void readyRead();

    void socketReadyRead();
    void socketConnected();
    void on_pushButtonSendName_clicked();
    void on_lineEditMessage_returnPressed();
};

#endif // MAINWINDOW_H
