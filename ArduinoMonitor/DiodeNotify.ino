/*
 Name:		DiodeNotify.ino
 Created:	5/7/2018 09:50:03
 Author:	Captain
*/
//0 -- ���������
//1 -- ��������� � ����� ���
//2 -- ������ ���������
int state = 0;

int notifyState = 0;

long longDelay = 500;
long shortDelay = 250;
long downDelay = 250;
long startTime = 0;
// the setup function runs once when you press reset or power the board
void setup() {
	pinMode(13, OUTPUT);
	Serial.begin(115200);
}

void checkState(int level, int Delay)
{
	if (millis() - startTime > Delay)
	{
		digitalWrite(13, level);
		if (++notifyState < 6)
			startTime = millis();
		else
		{
			notifyState = 0;
			state = 0;
		}
	}
}

void commonChat()
{
	switch (notifyState)
	{
		//������ �������
		case 0:
		{
			digitalWrite(13, HIGH);
			notifyState = 1;
			startTime = millis();
			break;
		}
		//������ ������� �������
		case 1:
		{
			checkState(LOW, longDelay);
			break;
		}
		//������ ������ �������
		case 2:
		{
			checkState(HIGH, downDelay);
			break;
		}
		//������ ������� �������
		case 3:
		{
			checkState(LOW, shortDelay);
			break;
		}
		//������ ������
		case 4:
		{
			checkState(HIGH, downDelay);
			break;
		}
		//������ ������� �������
		case 5:
		{
			checkState(LOW, longDelay);
			break;
		}
	}
}

//������ ���������
void privateMessage()
{
	switch (notifyState)
	{
		//������ �������
		case 0:
		{
			digitalWrite(13, HIGH);
			notifyState = 1;
			startTime = millis();
			break;
		}
		//������ ������� �������
		case 1:
		{
			checkState(LOW, shortDelay);
			break;
		}
		//������ ������ �������
		case 2:
		{
			checkState(HIGH, downDelay);
			break;
		}
		//������ ������� �������
		case 3:
		{
			checkState(LOW, shortDelay);
			break;
		}
		//������ ������
		case 4:
		{
			checkState(HIGH, downDelay);
			break;
		}
		//������ ������� �������
		case 5:
		{
			checkState(LOW, shortDelay);
			break;
		}
	}
}

// the loop function runs over and over again until power down or reset
void loop() {
	if (Serial.available() > 0)
	{
		char c = Serial.read();
		if (state == 0)
		{
			if (c == '1') state = 1;
			else if (c == '2') state = 2;
		}
	}

	if (state == 1)
		commonChat();
	else if (state == 2)
		privateMessage();

}
