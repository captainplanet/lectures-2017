#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QVBoxLayout>
#include <QScrollBar>
#include <QScrollArea>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QWidget* lblWidget = new QWidget();
    QVBoxLayout* lblLayout = new QVBoxLayout();
    lblLayout->addWidget(ui->labelChatOutput);
    lblWidget->setLayout(lblLayout);
    ui->scrollArea->setWidget(lblWidget);

    connect(ui->pushButtonConnect,
            SIGNAL(clicked(bool)),
            this,
            SLOT(connectClick()));
    connect(ui->pushButtonSend,
            SIGNAL(clicked(bool)),
            this,
            SLOT(sendClick()));

    port = new QSerialPort(this);
    connect(port,
            SIGNAL(readyRead()),
            this,
            SLOT(readyRead()));

    socket = new QTcpSocket(this);
    connect(socket,SIGNAL(readyRead()),this,SLOT(socketReadyRead()));
    connect(socket,SIGNAL(connected()),this,SLOT(socketConnected()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::connectClick()
{
    if (port->isOpen())
        port->close();

    QString portName = ui->lineEditPortName->text();
    port->setPortName(portName);
    if (port->open(QIODevice::ReadWrite))
    {
        port->setBaudRate(QSerialPort::Baud115200);
        port->setDataBits(QSerialPort::Data8);
        port->setFlowControl(QSerialPort::NoFlowControl);
        port->setParity(QSerialPort::NoParity);
        port->setStopBits(QSerialPort::OneStop);

        ui->pushButtonConnect->setStyleSheet("background-color:green;");
    }
    else
        ui->pushButtonConnect->setStyleSheet("background-color:red;");
}

void MainWindow::sendClick()
{
    if (port->isOpen())
    {
        QString command = ui->lineEditCommand->text();
        port->write(command.toLatin1());
    }
}

void MainWindow::readyRead()
{
    QByteArray data = port->readAll();
    QString labelText = ui->labelOutput->text();

    labelText += data;
    ui->labelOutput->setText(labelText);
}

void MainWindow::on_pushButtonSocketConnect_clicked()
{
    QString addressStr = ui->lineEditAddress->text();
    socket->connectToHost(QHostAddress(addressStr), 2018);
}

void MainWindow::on_pushButtonSendMessage_clicked()
{
    sendMessage();
}

void MainWindow::socketConnected()
{
    QString logText = ui->labelChatOutput->text();
    logText += "Connected\n";
    ui->labelChatOutput->setText(logText);
}

void MainWindow::socketReadyRead()
{
    while(socket->canReadLine())
    {
        QString sockStr = socket->readLine();

        //Команда для микроконтроллера
        char cmdForArduino = '1';
        if (sockStr.startsWith("!!!"))
                cmdForArduino = '2';
        if (port->isOpen())
            port->write(&cmdForArduino, 1);

        QString logText = ui->labelChatOutput->text();
        QString now = QDateTime::currentDateTime().time().toString();
        logText += "[" + now + "] " + sockStr;

        ui->labelChatOutput->setText(logText);

        ui->scrollArea->verticalScrollBar()->setSliderPosition(ui->labelChatOutput->height());
    }
}

void MainWindow::on_pushButtonSendName_clicked()
{
    if (socket->ConnectedState == QAbstractSocket::ConnectedState)
    {
        QString msg = "name;#" + ui->lineEditName->text() + "\n";
        socket->write(msg.toUtf8());
    }
}

void MainWindow::on_lineEditMessage_returnPressed()
{
    sendMessage();
}

void MainWindow::sendMessage()
{
    if (socket->ConnectedState == QAbstractSocket::ConnectedState)
    {
        QString text = ui->lineEditMessage->text();
        QString name;
        if (text[0] == '<')
        {
            QStringList strList = text.split(">");
            name = strList[0];
            name = name.replace(0,1,"");
            text = strList[1];
        }

        QString header = "text;#";
        if(name.size() != 0)
            header += name + ";#";
        QString msg = header + text + "\n";
        socket->write(msg.toUtf8());
        ui->lineEditMessage->clear();
    }
}
