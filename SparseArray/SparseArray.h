#include <list>
#include <iostream>
using namespace std;

template <typename Type>
struct SparseElement
{
	Type data;
	int index;
	SparseElement(int _index, Type _data = 0) : data(_data),
											index(_index) {}
};

template <typename Type>
class SparseArray
{
private:
	list<SparseElement<Type>> sparseList;
public:
	SparseArray() {}
	SparseArray(const SparseArray& arr);
	int size() const;
	Type& operator[](int idx);
	SparseArray& operator+=(const SparseArray& arr);
	Type operator*(const SparseArray& arr);
	friend ostream& operator<<(ostream&, const SparseArray&);
};

template <typename Type>
int SparseArray<Type>::size() const
{
	return sparseList.size();
}

template <typename Type>
SparseArray<Type>::SparseArray(const SparseArray& right)
{
	if (right.size() == 0) return;
	list<SparseElement<Type>>::iterator it = right.sparseList.begin();
	for (; it != right.sparseList.end(); ++it)
		sparseList.push_back(*it);
	sparseList.push_back(*it);
}

template <typename Type>
Type& SparseArray<Type>::operator[](int idx)
{
	//���� ������ ����
	if (sparseList.size() == 0)
	{
		sparseList.push_back(SparseElement<Type>(idx));
		return (*sparseList.begin()).data;
	}

	list<SparseElement<Type>>::iterator it = sparseList.begin();
	for (; it != sparseList.end(); ++it)
	{
		//������� ����������
		if ((*it).index == idx)
			return (*it).data;
		//������� �� ����������,
		//����� ��������� � ��������
		if ((*it).index > idx)
		{
			sparseList.insert(it, SparseElement<Type>(idx));
			return (*(--it)).data;
		}
	}
	//������� �� ����������, �����
	//��������� � �����
	sparseList.push_back(SparseElement<Type>(idx));
	return (*(--sparseList.end())).data;
}

template <typename Type>
SparseArray<Type>& SparseArray<Type>::operator+=(const SparseArray<Type>& right)
{
	list<SparseElement<Type>>::const_iterator it = right.sparseList.begin();
	for (; it != right.sparseList.end(); ++it)
		//operator[]((*it).index) += (*it).data;
		(*this)[(*it).index] += (*it).data;
	return *this;
}

template <typename Type>
Type SparseArray<Type>::operator*(const SparseArray<Type>& right)
{
	Type sum = 0;
	list<SparseElement<Type>>::const_iterator it = right.sparseList.begin();
	for (; it != right.sparseList.end(); ++it)
		sum += (*this)[(*it).index] * (*it).data;
	return sum;
}