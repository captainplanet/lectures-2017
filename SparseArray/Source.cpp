#include <iostream>
#include "SparseArray.h"
using namespace std;

int main()
{
	SparseArray<double> sparseArray1;
	sparseArray1[0] = 1;
	sparseArray1[2] = 5;
	sparseArray1[10] = 9;

	SparseArray<double> sparseArray2;
	sparseArray2[0] = 1;
	sparseArray2[3] = 5;
	sparseArray2[8] = 9;
	sparseArray2 += sparseArray1;

	for (int i = 0; i < 12; ++i)
		cout << sparseArray2[i] << endl;
	cout << sparseArray1*sparseArray2 << endl;
	//list<int> testList;
	//testList.push_back(1);
	//testList.push_back(3);
	//testList.insert(testList.begin(), 2);
	//list<int>::iterator it = testList.begin();
	//for (; it != testList.end();++it)
	//	cout << (*it) << endl;
	system("pause");
	return 0;
}