#include "mainwindow.h"
#include "ui_mainwindow.h"

/// TODO:
/// ПРОВЕРКА ОТКЛЮЧЕНИЯ КЛИЕНТОВ

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    server = new QTcpServer(this);
    if (server->listen(QHostAddress::Any, 2018))
    {
        ui->labelStatus->setText("Server is on");
    }
    else
    {
        ui->labelStatus->setText("Server failed to start");
    }

    connect(server,SIGNAL(newConnection()),this,SLOT(onConnection()));

    socketList = new QList<QTcpSocket*>();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onConnection()
{
    QTcpSocket* newConnection =server->nextPendingConnection();
    connect(newConnection,SIGNAL(disconnected()),this,SLOT(onSocketDisconnect()));

    //Проверка на наличие сокета в списке
    for (QTcpSocket* client : *socketList)
        if (client->peerAddress() == newConnection->peerAddress())
        {
            socketList->removeOne(client);
            break;
        }

    socketList->push_back(newConnection);
    connect(newConnection,SIGNAL(readyRead()),this,SLOT(readyRead()));
    QString logText = ui->labelLog->text();
    logText += "\n" + newConnection->peerAddress().toString();
    ui->labelLog->setText(logText);
}

void MainWindow::onSocketDisconnect()
{
    QTcpSocket* senderSocket = qobject_cast<QTcpSocket*>(QObject::sender());
    socketList->removeOne(senderSocket);
}

void MainWindow::readyRead()
{
    QTcpSocket* senderSocket = qobject_cast<QTcpSocket*>(QObject::sender());
    while(senderSocket->canReadLine())
    {
        QString str = senderSocket->readLine();

        QStringList strList = str.split(";#");
        if (strList[0].contains("name"))
        {
            QString name = strList[1].trimmed();
            if (name.size() == 0)
            {
                senderSocket->write(QString("Нельзя задавать пустое имя\n").toUtf8());
                break;
            }

            for (QTcpSocket* client : *socketList)
                if (client->objectName() == name)
                {
                    senderSocket->write(QString("Такой пользователь уже есть\n").toUtf8());
                    return;
                }

            QString prevName = senderSocket->objectName();

            senderSocket->setObjectName(name.replace("\n",""));
            if (prevName.size() == 0)
                prevName = senderSocket->peerAddress().toString();
            QString msg = prevName + " теперь зовется " + name +"\n";

            for (QTcpSocket* client : *socketList)
                client->write(msg.toUtf8());

            break;
        }



        if (senderSocket->objectName().size() == 0)
        {
            senderSocket->write(QString("Быть анонимом плохо\n").toUtf8());
            break;
        }

        QString name;
        QString text;
        if (strList.size() > 2)
        {
            name = strList[1];
            text = strList[2];
        }
        else
            text = strList[1];

        str = senderSocket->objectName()+ ": " + text;
        if (name.size() == 0)
            for (QTcpSocket* client : *socketList)
                client->write(str.toUtf8());
        else
            for (QTcpSocket* client : *socketList)
                if (client->objectName() == name)
                {
                    str = "!!! " + str;
                    client->write(str.toUtf8());
                    senderSocket->write(str.toUtf8());
                    break;
                }
    }
}
