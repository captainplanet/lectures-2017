
class Shape2d
{
public:
	virtual double getArea() = 0;
	virtual double getPerimeter() = 0;
	virtual Point getCenter() = 0;
};

class Polygon : public Shape2d
{
protected:
	Point* vertices;
	int size;
};

class Ellipse : public Shape2d
{
protected:
	double a;
	double b;
	Point center;
	double angle;
};