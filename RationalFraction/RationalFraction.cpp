#include "RationalFraction.h"
#include <math.h>
#include <iostream>
using namespace std;

RationalFraction::RationalFraction(double decimal, int _precision)
{
	fraction = decimal;
	precision = _precision;
	double intPart = 0;
	double fractPart = 0;
	fractPart = modf(decimal, &intPart);
	//�� ����������
	denumerator = pow(10, precision);
	numerator = round(fraction*denumerator);
	int gcd = gcdEuclid(numerator, denumerator);
	numerator /= gcd;
	denumerator /= gcd;
}

RationalFraction::RationalFraction(int _num, int _den)
{
	numerator = _num;
	denumerator = _den;
	int gcd = gcdEuclid(numerator, denumerator);
	numerator /= gcd;
	denumerator /= gcd;

	fraction = (double)numerator / denumerator;
	precision = 0;
}

double RationalFraction::getFractionalPart() const
{
	double intPart = 0;
	return modf(fraction, &intPart);
}

RationalFraction RationalFraction::getRationalFractionalPart() const
{
	return RationalFraction(numerator%denumerator, denumerator);
}

RationalFraction& RationalFraction::operator+=(const RationalFraction &a)
{
	*this = *this + a;
	return *this;
}

RationalFraction & RationalFraction::operator++()
{
	numerator += denumerator;
	return *this;
}

RationalFraction RationalFraction::operator++(int n)
{
	RationalFraction tmp = *this;
	numerator += denumerator;
	return tmp;
}

int RationalFraction::gcdEuclid(int a, int b) const
{
	int tmp;
	if (abs(a) < abs(b))
	{
		tmp = b;
		b = a;
		a = tmp;
	}

	while (b != 0)
	{
		tmp = b;
		b = a % b;
		a = tmp;
	}
	return a;
}

ostream& operator<<(ostream& out, RationalFraction& fraction)
{
	out << fraction.getNumerator() << "/" << fraction.getDenumerator() << endl;
	return out;
}

RationalFraction operator+(RationalFraction a, RationalFraction b)
{
	int numerator = a.getNumerator()*b.getDenumerator() 
					+ b.getNumerator()*a.getDenumerator();
	int denumerator = a.getDenumerator()*b.getDenumerator();

	return RationalFraction(numerator, denumerator);
}

//�������� ����� � ����� ������
RationalFraction operator+(RationalFraction a, int b)
{
	int numerator = a.getNumerator() + b*a.getDenumerator();

	return RationalFraction(numerator, a.getDenumerator());
}