#include <iostream>
#include "RationalFraction.h"
using namespace std;

int main()
{
	RationalFraction fraction(0.1, 1);
	RationalFraction fraction2(0.2,1);

	cout << fraction + fraction2 << endl;
	cout << fraction + 5 << endl;
	cout << ((fraction += fraction2) += fraction2) << endl;
	cout << ++fraction << endl;
	cout << fraction++ << endl;
	cout << fraction << endl;

	system("pause");
	return 0;
}