#include <iostream>
using namespace std;
//ostream& operator<<(ostream& out, RationalFraction& fraction);

class RationalFraction
{
public:
	RationalFraction() : denumerator(1) {}
	RationalFraction(double decimal, int precision);
	RationalFraction(int _num, int _den);

	double getFraction() const { return fraction; }
	int getNumerator() const { return numerator; }
	int getDenumerator() const { return denumerator; }
	int getInteger() const { return numerator / denumerator; }
	int getPrecision() const { return precision; }
	double getFractionalPart() const;
	//������� ������� ����� � ���� ������������ �����
	RationalFraction getRationalFractionalPart() const;

	friend ostream& operator<<(ostream& out, RationalFraction& fraction);
	RationalFraction& operator+=(const RationalFraction&);
	RationalFraction& operator++();
	RationalFraction operator++(int);
	//friend RationalFraction operator+(RationalFraction a, RationalFraction b);

private:
	double fraction;
	int numerator;
	int denumerator;
	//����� ������ ����� �������
	//0, ���� �������� ����� �����������
	//��������� ��� ���� double
	int precision;
	int gcdEuclid(int a, int b) const;
};

RationalFraction operator+(RationalFraction a, RationalFraction b);
RationalFraction operator+(RationalFraction a, int b);