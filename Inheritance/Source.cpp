#include <iostream>
using namespace std;

class A
{
public:
	int xPub;
	A() { cout << "A!" << endl; }
	A(int _xPub) { xPub = _xPub; }
private:
	int xPriv;
protected:
	int xProt;
	int& getXPriv() { return xPriv; }
};

class B : public A
{
public:
	B() : A(2)
	{
		xPub = 1;
	}

	void foo()
	{
		xPub = 1;
		xProt = 2;
		
	}
};

class C : protected A
{
public:
	void foo()
	{
		xPub = 1;
	}
};

class Cc : public C
{
public:
	void foo()
	{
		xProt = 1;
	}
};

class D : private A
{
public:
	void foo()
	{
		xPub = 1;
		xPriv = 1;
		xProt = 2;
	}
};

int main()
{
	B b;
	b.xPub = 10;

	C c;
	c.xPub = 2;
	system("pause");
	return 0;
}