// MainArgs.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
using namespace std;

int main(int argc, char* argv[], char* env[])
{
	cout << "Hello world!" << endl;
	cout << "Arguments count: " << argc << endl;
	cout << "Commandline arguments: " << endl;
	for (int i = 0; i < argc; ++i)
		cout << argv[i] << endl;
	if (argc > 2)
	{
		int x = atoi(argv[1]);
		cout << x*x << endl;

		double f = atof(argv[2]);
		cout << f / 2 << endl;
	}

	/*int i = 0;
	cout << "Environment variables: " << endl;
	while (env[i] != NULL)
	{
		cout << env[i] << endl;
		++i;
	}*/
	system("pause");
    return 0;
}

