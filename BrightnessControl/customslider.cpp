#include "customslider.h"
#include <QStyleOption>
#include <QMouseEvent>
#include <QDebug>

CustomSlider::CustomSlider(QWidget *parent) : QWidget(parent)
{
    setStyleSheet("border: 1px solid black;");
    height = 0.5;
    mouseButtonState = false;
    sliderWidth = 0.1;
}

void CustomSlider::paintEvent(QPaintEvent *event)
{
    QSize currSize = size();
    QPainter painter(this);
    //Основной уровень
    painter.setBrush(QBrush(QColor(0,255,0)));
    painter.setPen(Qt::NoPen);
    int upperLevel = currSize.height()*(1 - height);

    int actualSliderWidth = sliderWidth*currSize.height();
    //Ограничение на верхнюю границу
    if (upperLevel - actualSliderWidth/2 < 0)
        upperLevel = actualSliderWidth/2;
    if (upperLevel + actualSliderWidth/2 > currSize.height())
        upperLevel = currSize.height() - actualSliderWidth/2;

    painter.drawRect(0,
                     upperLevel,
                     currSize.width(),
                     height*currSize.height());
    //Слайдер
    painter.setPen(Qt::black);
    painter.setBrush(QBrush(QColor(128,128,128)));
    painter.drawRect(0,
                     upperLevel - actualSliderWidth/2,
                     currSize.width(),
                     actualSliderWidth);

    QStyleOption opt;
    opt.init(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &painter, this);

}

void CustomSlider::mouseMoveEvent(QMouseEvent *event)
{
    if (mouseButtonState)
    {
        //Фактическая ширина слайдера в пикселях
        int actualSliderWidth = sliderWidth*size().height();
        height = 1 - (double)event->pos().y()
                /(size().height()-actualSliderWidth);
        height = height > 1 ? 1 : height;
        height = height < 0 ? 0 : height;

        emit valueChanged(height);
        repaint();
    }
}

void CustomSlider::mousePressEvent(QMouseEvent *event)
{
    //Координата Y мышки
    QPoint mousePos = event->pos();
    int mouseY = mousePos.y();

    QSize currSize = size();
    //Фактическая ширина слайдера в пикселях
    int actualSliderWidth = sliderWidth*currSize.height();
    //Текущий уровень в пикселях
    int currLevel = (currSize.height()-actualSliderWidth)*(1 - height);
    //Границы
    int upperBound = currLevel;
    int lowerBound = currLevel + actualSliderWidth;

    if (mouseY > upperBound && mouseY < lowerBound)
        mouseButtonState = true;
}

void CustomSlider::mouseReleaseEvent(QMouseEvent *event)
{
   mouseButtonState = false;
}
