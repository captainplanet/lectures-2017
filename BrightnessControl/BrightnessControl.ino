/*
 Name:		BrightnessControl.ino
 Created:	5/21/2018 09:44:35
 Author:	Captain
*/

// the setup function runs once when you press reset or power the board
void setup() {
	Serial.begin(115200);
	pinMode(13, OUTPUT);
}

// the loop function runs over and over again until power down or reset
void loop() {
	if (Serial.available() >= 1)
	{
		char val[1];
		Serial.readBytes(val, 1);
		int brightness = 255 * (double)val[0] / 100;
		analogWrite(13, brightness);
	}
	//delay(50);
}
