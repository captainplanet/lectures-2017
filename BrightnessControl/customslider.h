#ifndef CUSTOMSLIDER_H
#define CUSTOMSLIDER_H

#include <QWidget>
#include <QPainter>

class CustomSlider : public QWidget
{
    Q_OBJECT
    public:
        explicit CustomSlider(QWidget *parent = nullptr);

    private:
        bool mouseButtonState;
        double height;
        double sliderWidth;
    signals:
        void valueChanged(double value);

    public slots:

protected:
    void paintEvent(QPaintEvent *event);
    void mousePressEvent(QMouseEvent* event);
    void mouseReleaseEvent(QMouseEvent* event);
    void mouseMoveEvent(QMouseEvent* event);
};

#endif // CUSTOMSLIDER_H
