#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "customslider.h"
#include <QLayout>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    CustomSlider *slider = new CustomSlider(ui->centralWidget);
    slider->setGeometry(10,10,100,200);

    connect(slider,SIGNAL(valueChanged(double)),this,SLOT(onSliderChange(double)));

    port = new QSerialPort(this);
}

void MainWindow::onSliderChange(double value)
{
    ui->label->setText(QString::number(value));
    char percents = value*100;
    if (port->isOpen())
        port->write((char*)&percents, 1);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    if (port->isOpen())
        port->close();

    QString portName = ui->lineEdit->text();
    port->setPortName(portName);
    if (port->open(QIODevice::ReadWrite))
    {
        port->setBaudRate(QSerialPort::Baud115200);
        port->setDataBits(QSerialPort::Data8);
        port->setFlowControl(QSerialPort::NoFlowControl);
        port->setParity(QSerialPort::NoParity);
        port->setStopBits(QSerialPort::OneStop);

        ui->pushButton->setStyleSheet("background-color:green;");
    }
    else
        ui->pushButton->setStyleSheet("background-color:red;");
}
