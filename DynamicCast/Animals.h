#include <iostream>
using namespace std;

class Animal
{
public:
	Animal() { cout << "Animal constructor" << endl; }
	virtual ~Animal() { cout << "Killing the animal" << endl; }
	virtual void move(int meters = 1)
	{
		cout << "Some animal moves " << meters << " meters" << endl;
	}
	virtual void eat()
	{
		cout << "Animal eats" << endl;
	}
	virtual void sleep() final
	{
		cout << "Animal sleeps" << endl;
	}
	//������ ����������� �������
	//���������� ��������� �� ��������������
	//��� ���� ������� ����� ���������� �����������
	//virtual void sleep1() = 0;
};

class Capybara : public Animal
{
public:
	int y;
	Capybara() { cout << "Capybara constructor" << endl; y = 2; }
	~Capybara() { cout << "Killing the capybara" << endl; }
	void move(int meters = 2) override
	{
		cout << "Capybara moves " << meters << " meters" << endl;
	}
	void eat()
	{
		cout << "Capybara eats" << endl;
	}
	void GiveBirth()
	{
		cout << "Little capybara have been born" << endl;
	}
};

class Platypus : public Animal
{
public:
	int x;
	Platypus() { cout << "Platypus constructor" << endl; }
	~Platypus() { cout << "Killing the platypus" << endl; }
	void move(int meters = 3) override
	{
		cout << "Platypus moves " << meters << " meters" << endl;
	}
	void eat()
	{
		cout << "Platypus eats" << endl;
	}
	void LayEgg()
	{
		cout << "Little platypus' egg have been laid" << endl;
	}
};