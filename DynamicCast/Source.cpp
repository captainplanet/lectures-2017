#include <iostream>
#include "Animals.h"
using namespace std;

void FeedAnimal(Animal* animal)
{
	animal->eat();
	Capybara* c = dynamic_cast<Capybara*>(animal);
	if (c != nullptr)
	{
		c->GiveBirth();
	}
	Platypus* p = dynamic_cast<Platypus*>(animal);
	if (p != nullptr)
	{
		p->LayEgg();
	}
}

int main()
{
	Animal* capybara = new Capybara();
	Animal* platypus = new Platypus();

	Animal a;
	Capybara c;
	a = c;

	capybara->move();
	platypus->move();
	
	FeedAnimal(capybara);
	FeedAnimal(platypus);

	delete capybara;
	delete platypus;

	system("pause");
	return 0;
}