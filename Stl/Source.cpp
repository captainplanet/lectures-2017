#include <iostream>
#include <string>
#include <sstream>
#include <set>
#include <algorithm>
#include <iterator>
#include <vector>
#include <map>

using namespace std;

int main()
{
	string str1 = "Hello";
	string str2 = " world!";
	string str = str1 + str2;
	string strCopy;
	strCopy = str1;
	cout << str << endl;
	strCopy[0] = 'h';
	cout << str1 << endl;
	cout << strCopy << endl;

	int n = str1.find("Hel", 0);
	cout << n << endl;

	string newStr = str1.replace(0, 2, "Al");
	cout << str1 << endl;
	cout << newStr << endl;
	//�������������� �����
	//�������1
	string megaString1 = to_string(10) + " apples, weight: " +
						to_string(1.2) + " kg";
	cout << megaString1 << endl;

	//�������2
	char buf[100];
	snprintf(buf, sizeof(buf), "%d apples, weight: %f kg", 10, 1.2);
	string strFromStd = buf;
	cout << strFromStd << endl;

	//�������3
	stringstream ss;
	ss << 10 << " apples, weight: " << 1.2 << " kg";
	string sstreamStr = ss.str();
	cout << sstreamStr << endl;
	
	stringstream ss1("10.2");
	double fromString;
	ss1 >> fromString;
	cout << fromString << endl;
	double fromString2;
	ss1 >> fromString2;
	cout << fromString2 << endl;

	//���������� STL
	cout << endl << "-------------------" << endl << endl;
	cout << "SET" << endl;
	set<int> setStl;
	setStl.insert(5);
	setStl.insert(1);
	setStl.insert(2);
	setStl.insert(3);
	setStl.insert(3);
	set<int>::iterator it = setStl.begin();
	for (; it != setStl.end(); ++it)
		cout << (*it) << endl;

	set<int> setStl2;
	setStl.insert(2);
	setStl.insert(3);

	vector<int> vec1{ 1,3, 4, 4,5 };
	vector<int> vec2{ 1, 4, 4, 5 };
	vector<int> intersection;
	vector<int> unionVec;
	set_intersection(vec1.begin(), vec1.end(),
					vec2.begin(), vec2.end(),
					back_inserter(intersection));
	set_union(vec1.begin(), vec1.end(),
				vec2.begin(), vec2.end(),
				back_inserter(unionVec));
	cout << "Intersection" << endl;
	for (int n : intersection)
		cout << n << endl;

	cout << "Union" << endl;
	for (int &n : unionVec)
	{
		cout << n << endl;
		n = 0;
	}
	for (int n : unionVec)
		cout << n << endl;
	
	setlocale(LC_ALL, "rus");
	map<string, string> stringMap;
	stringMap["apple"] = "������";
	stringMap["shot"] = "�������";
	cout << stringMap["apple"] << endl;
	cout << "MAP" << endl;
	for (pair<string, string> word : stringMap)
		cout << word.first << " : " << word.second << endl;

	cout << "MULTIMAP" << endl;
	multimap<int, string> multimapStl;
	multimapStl.insert(pair<int, string>(1, "ivanov"));
	multimapStl.insert(pair<int, string>(1, "sidorov"));
	multimapStl.insert(pair<int, string>(2, "petrov"));
	multimapStl.insert(pair<int, string>(2, "vasiliev"));
	multimapStl.insert(pair<int, string>(3, "chapaev"));
	for (int i = 1; i <= 3; ++i)
	{
		cout << "������ " << i << endl;
		multimap<int, string>::iterator it1 = multimapStl.lower_bound(i);
		multimap<int, string>::iterator it2 = multimapStl.upper_bound(i);
		for (; it1 != it2; ++it1)
			cout << "\t" << (*it1).second << endl;
	}

	for (pair<int, string> student : multimapStl)
		cout << student.second << " : " << student.first << endl;

	
	system("pause");
	return 0;
}