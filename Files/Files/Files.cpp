#include <iostream>
#include <fstream>
using namespace std;

void writeToFileStream(ofstream& fout, char* str)
{
	fout << str << endl;
}

int main()
{
	ofstream fout;
	ifstream fin;

	fout.open("out.txt");
	if (!fout.is_open())
	{
		cout << "Failed to open out file" << endl;
		system("pause");
		return -1;
	}

	fin.open("in.txt");
	if (!fin.is_open())
	{
		cout << "Failed to open in file" << endl;
		system("pause");
		return -1;
	}

	while (!fin.eof())
	{
		char str[64];
		char c;
		fin >> str;
		c = (char)fin.get();
		fout << str;
		if (c != EOF)
			fout << c;
	}

	writeToFileStream(fout, "��������������������������������");
	fout.close();
	fin.close();
	system("pause");
	return 0;
}