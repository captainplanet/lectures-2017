/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QPushButton *pushButtonConnect;
    QPushButton *pushButtonSend;
    QLineEdit *lineEditPortName;
    QLineEdit *lineEditCommand;
    QLabel *labelOutput;
    QLineEdit *lineEditMessage;
    QLineEdit *lineEditAddress;
    QLabel *labelChatOutput;
    QPushButton *pushButtonSendMessage;
    QPushButton *pushButtonSocketConnect;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(695, 178);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        pushButtonConnect = new QPushButton(centralWidget);
        pushButtonConnect->setObjectName(QStringLiteral("pushButtonConnect"));
        pushButtonConnect->setGeometry(QRect(10, 40, 75, 23));
        pushButtonSend = new QPushButton(centralWidget);
        pushButtonSend->setObjectName(QStringLiteral("pushButtonSend"));
        pushButtonSend->setGeometry(QRect(10, 90, 75, 23));
        lineEditPortName = new QLineEdit(centralWidget);
        lineEditPortName->setObjectName(QStringLiteral("lineEditPortName"));
        lineEditPortName->setGeometry(QRect(10, 10, 71, 20));
        lineEditCommand = new QLineEdit(centralWidget);
        lineEditCommand->setObjectName(QStringLiteral("lineEditCommand"));
        lineEditCommand->setGeometry(QRect(10, 70, 71, 20));
        labelOutput = new QLabel(centralWidget);
        labelOutput->setObjectName(QStringLiteral("labelOutput"));
        labelOutput->setGeometry(QRect(90, 10, 111, 101));
        labelOutput->setStyleSheet(QStringLiteral("border: 1px solid black;"));
        lineEditMessage = new QLineEdit(centralWidget);
        lineEditMessage->setObjectName(QStringLiteral("lineEditMessage"));
        lineEditMessage->setGeometry(QRect(240, 70, 91, 20));
        lineEditAddress = new QLineEdit(centralWidget);
        lineEditAddress->setObjectName(QStringLiteral("lineEditAddress"));
        lineEditAddress->setGeometry(QRect(240, 10, 91, 20));
        labelChatOutput = new QLabel(centralWidget);
        labelChatOutput->setObjectName(QStringLiteral("labelChatOutput"));
        labelChatOutput->setGeometry(QRect(340, 10, 351, 101));
        labelChatOutput->setStyleSheet(QStringLiteral("border: 1px solid black;"));
        labelChatOutput->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        pushButtonSendMessage = new QPushButton(centralWidget);
        pushButtonSendMessage->setObjectName(QStringLiteral("pushButtonSendMessage"));
        pushButtonSendMessage->setGeometry(QRect(240, 90, 91, 23));
        pushButtonSocketConnect = new QPushButton(centralWidget);
        pushButtonSocketConnect->setObjectName(QStringLiteral("pushButtonSocketConnect"));
        pushButtonSocketConnect->setGeometry(QRect(240, 40, 91, 23));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 695, 21));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        pushButtonConnect->setText(QApplication::translate("MainWindow", "Connect", 0));
        pushButtonSend->setText(QApplication::translate("MainWindow", "Send", 0));
        lineEditPortName->setText(QApplication::translate("MainWindow", "COM13", 0));
        labelOutput->setText(QString());
        lineEditAddress->setText(QApplication::translate("MainWindow", "192.168.43.59", 0));
        labelChatOutput->setText(QString());
        pushButtonSendMessage->setText(QApplication::translate("MainWindow", "Send", 0));
        pushButtonSocketConnect->setText(QApplication::translate("MainWindow", "Connect", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
