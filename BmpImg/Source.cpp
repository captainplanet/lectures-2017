#include <iostream>
#include <fstream>
#include <Windows.h>
using namespace std;

struct Rgb
{
	unsigned char B;
	unsigned char G;
	unsigned char R;
};

int main()
{
	BITMAPFILEHEADER fh;
	BITMAPINFOHEADER bi;
	fstream img;
	img.open("img.bmp", ios::binary | ios::in);
	//TODO: check if file is open
	img.read((char*)&fh, sizeof(fh));
	img.read((char*)&bi, sizeof(bi));

	cout << "File info: " << endl;
	cout << "File size: " << fh.bfSize << endl;
	cout << "File type: " << (char)(fh.bfType & 0xFF) 
		 << (char)(fh.bfType >> 8) << endl;
	cout << "Offset: " << fh.bfOffBits << endl;

	cout << "Image info: " << endl;
	cout << "Image size in bytes: " << bi.biSizeImage << endl;
	cout << "Image width: " << bi.biWidth << endl;
	cout << "Image height: " << bi.biHeight << endl;
	cout << "Image color depth: " << bi.biBitCount << endl;

	int mod = (3 * bi.biWidth) % 4;
	int offset = mod == 0 ? 0 : 4 - mod;
	Rgb** pixels = new Rgb*[bi.biHeight];
	for (int i = 0; i < bi.biHeight; ++i)
	{
		pixels[i] = new Rgb[bi.biWidth];
		for (int j = 0; j < bi.biWidth;++j)
			img.read((char*)&pixels[i][j], 3);
		img.seekg(offset, ios::cur);
	}

	//������ ������ � ����� -- ��������� ������ �����������
	for (int j = 0; j < bi.biWidth; ++j)
	{
		pixels[0][j].R = 0;
		pixels[0][j].G = 0;
		pixels[0][j].B = 255;
	}

	fstream outImg;
	outImg.open("out.bmp", ios::binary | ios::out);
	//TODO: check if file is open
	outImg.write((char*)&fh, sizeof(fh));
	outImg.write((char*)&bi, sizeof(bi));
	
	char* offsetBytes = new char[offset];
	for (int i = 0; i < offset; ++i)
		offsetBytes[i] = 0;

	for (int i = 0; i < bi.biHeight; ++i)
	{
		for (int j = 0; j < bi.biWidth; ++j)
			outImg.write((char*)&pixels[i][j], 3);
		outImg.write(offsetBytes, offset);
	}

	img.close();
	outImg.close();
	for (int i = 0; i < bi.biHeight; ++i)
		delete[] pixels[i];
	delete[] pixels;
	delete[] offsetBytes;
	system("pause");
	return 0;
}