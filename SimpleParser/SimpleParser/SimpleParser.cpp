#include <iostream>
using namespace std;

int main()
{
	setlocale(LC_ALL, "Ru");
	double left = 0;
	double right = 0;
	double result = 0;
	char sign = 0;
	cout << "������� ���������" << endl;
	cin >> left;
	cin >> sign;
	cin >> right;

	switch (sign)
	{
		case '+': 
		{
			result = left + right;
			break;
		}
		case '-':
		{
			result = left - right;
			break;
		}
		case '*':
		{
			result = left * right;
			break;
		}
		case '/':
		{
			result = left / right;
			break;
		}
		default:
		{
			cout << "Wrong input" << endl;
			system("pause");
			return -1;
		}
	}

	cout << "Result = " << result << endl;
	system("pause");
	return 0;
}