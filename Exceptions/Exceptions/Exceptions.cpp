#include <iostream>
using namespace std;

int GCD(int x, int y)
{
	if (x <= 0)
		throw "First argument is negative";
	if (y <= 0)
		throw "Second argument is negative";

	while (x > 0 && y > 0)
	{
		if (x > y)
			x %= y;
		else
			y %= x;
	}
	return x > 0 ? x : y;
}

int main()
{
	while (true)
	{
		int x;
		int y;
		cout << "Input first number: ";
		cin >> x;
		cout << "Input second number: ";
		cin >> y;

		try
		{
			cout << GCD(x, y) << endl;
		}
		catch (char error[])
		{
			cout << "Error: " << error << endl;
		}

		char c;
		cout << "Continue? y\\n ";
		cin >> c;
		if (c == 'n') break;
	}
	//system("pause");
	return 0;
}