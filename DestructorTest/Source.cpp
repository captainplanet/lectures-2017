#include <iostream>
using namespace std;

class T
{
public:
	T(const T& from)
	{
		cout << "Copy" << endl;
		size = from.size;
		data = new int[size];
		for (int i = 0; i < size; ++i)
			data[i] = from.data[i];
	}

	T()
	{
		cout << "Default" << endl;
		size = 3;
		data = new int[size];
		for (int i = 0; i < size; ++i)
			data[i] = i;
	}

	~T() 
	{ 
		cout << "SYMPHONY OF DESTRUCTION!!!" << endl;
		delete[] data; 
	}
	int* data;
	int size;
};

int* ptr;

T& foo()
{
	T* f = new T();
	ptr = f->data;
	return *f;
}

void bar()
{
	T f = foo();
}

int main()
{
	T t = foo();
	T f;
	cout << "Data: " << ptr[1] << endl;
	f = foo();

	bar();
	cout << "Data: " << ptr[1] << endl;
	system("pause");
	return 0;
}