#include <iostream>
using namespace std;

int main()
{
	double x1 = 0;

	double dx = 1/10;
	double tolerance = 0.0001;
	cout.precision(32);
	while (x1 < 2)
	{
		cout << x1 << "\t0.8\t" << (abs(x1 - 0.8) < tolerance) << endl;
		x1 += dx;
	}

	system("pause");
	return 0;
}