#include <iostream>
#include <functional>
#include <algorithm>
using namespace std;

typedef double(*fp)(double);

template <typename Type, typename Type2>
Type max(Type a, Type2 b)
{
	static int count = 0;
	++count;

	cout << "Counter = " << count << endl;
	return a > b ? a : b;
}

double sqr(double x)
{
	return x*x;
}

double increment(double x)
{
	return x + 1;
}

double integral(
	double(*fPtr)(double),
	double a,
	double b,
	int count)
{
	double sum = 0;
	double dx = (b - a) / count;
	for (int i = 0; i < count; ++i)
	{
		sum += fPtr(a + dx / 2)*dx;
		a += dx;
	}
	return sum;
}

int tmp = 10;
function<double(double)> sumOfFunctions(fp sqrF, fp incrF)
{
	return [=](double x) { return sqrF(x) + incrF(x) + tmp; };
}

int main()
{
	char str[32] = "hello";
	cout << max<int>(5, 3) << endl;
	cout << max<double>(5.5,3.0) << endl;

	double (*fPtr)(double) = sqr;
	cout << fPtr(5) << endl;
	
	cout << "Size of fPtr: " << sizeof(fPtr) << endl;
	cout << "Integral = " << integral(fPtr, -3, 3, 10) << endl;
	cout << "Integral = " << integral(fPtr, -3, 3, 100) << endl;
	cout << "Integral = " << integral(fPtr, -3, 3, 1000) << endl;
	
	fp fpArray[3];
	fpArray[0] = sqr;
	cout << fpArray[0](4) << endl;
	
	auto lambda = [=](double x, double y) { return x + y; };
	cout << lambda(1, 2) << endl;

	auto lambdaSum = sumOfFunctions(sqr, increment);
	tmp = 15;
	cout  << "Sum of funcs =  " << lambdaSum(6) << endl;

	int testArray[] = { 3,1,2 };
	

	//Leads to comparison error
	//cout << max<int,char*>(1, str) << endl;
	system("pause");
	return 0;
}