#include <iostream>
#include <time.h>
using std::cout;
using std::cin;
using std::endl;

int main()
{
	//Initialize random generator
	srand(time(NULL));

	const int statSize = 100000;
	int min = 5;
	int max = 15;
	int statistics[statSize];
	double average = 0;
	double dispersion = 0;

	//OPERATIONS BLOCK
	for (int i = 0; i < statSize; ++i)
		statistics[i] = min + rand() % (max-min+1);

	for (int i = 0; i < statSize; ++i)
		average += statistics[i];
	average /= statSize;

	for (int i = 0; i < statSize; ++i)
		dispersion += (statistics[i] - average)*(statistics[i] - average);
	dispersion = sqrt(dispersion/statSize);

	//OUTPUT BLOCK
	/*for (int i = 0; i < statSize; ++i)
		cout << statistics[i] << endl;
	cout << endl;*/

	cout << "Average: " << average << endl << endl;
	cout << "Dispersion: " << dispersion << endl;

	system("pause");
	return 0;
}