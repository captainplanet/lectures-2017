#include <iostream>
using namespace std;

template <typename Type>
struct TreeNode
{
	TreeNode<Type>* parent;
	TreeNode<Type>* left;
	TreeNode<Type>* right;
	Type data;
	TreeNode(Type _data) :	left(nullptr),
							right(nullptr),
							parent(nullptr),
							data(_data) {}
};

template <typename Type>
class BinaryTree
{
private:
	TreeNode<Type>* root;
	int size;

	void addItemSubtree(Type item, TreeNode<Type>* subroot);
	void deleteSubtree(TreeNode<Type>* subroot);
	bool findItemSubtree(Type item, TreeNode<Type>*& result, TreeNode<Type>* subroot);
	void printSubtree(TreeNode<Type>* subroot);

public:
	BinaryTree() : root(nullptr), size(0) {}
	~BinaryTree();
	void addItem(Type item);
	bool removeItem(Type item);
	bool findItem(Type item, Type& result);
	void print();
	
};

template<typename Type>
BinaryTree<Type>::~BinaryTree()
{
	if (size == 0) return;
	deleteSubtree(root);
}

template<typename Type>
void BinaryTree<Type>::addItem(Type item)
{
	if (size == 0)
	{
		root = new TreeNode<Type>(item);
		++size;
		return;
	}
	addItemSubtree(item, root);
}

template<typename Type>
void BinaryTree<Type>::print()
{
	if (size == 0) return;
	printSubtree(root);
}

template <typename Type>
bool BinaryTree<Type>::findItem(Type item, Type& result)
{
	if (size == 0) return false;
	TreeNode<Type>* foundNode;
	bool found = findItemSubtree(item, foundNode, root);
	if (found)
		result = foundNode->data;
	return found;
}

template <typename Type>
bool BinaryTree<Type>::removeItem(Type item)
{
	TreeNode<Type>* foundNode;
	bool found = findItemSubtree(item, foundNode, root);
	if (!found) return false;

	//���� ���� �������� ������
	if (foundNode->left == nullptr && foundNode->right == nullptr)
	{
		if (foundNode->parent->left == foundNode)
			foundNode->parent->left = nullptr;
		else
			foundNode->parent->right = nullptr;

		return true;
	}

	//���� � ���� ���� ��� �������
	if (foundNode->left != nullptr && foundNode->right != nullptr)
	{
		TreeNode<Type>* maxNode = foundNode->left;
		while (maxNode->right != nullptr)
			maxNode = maxNode->right;
		foundNode->data = maxNode->data;
		if (maxNode->parent->left == maxNode)
			maxNode->parent->left = nullptr;
		else
			maxNode->parent->right = nullptr;
		delete maxNode;

		return true;
	}

	//���� � ���� ������������ ������� �����
	if (foundNode->left != nullptr)
	{
		if (foundNode->parent->left == foundNode)
			foundNode->parent->left = foundNode->left;
		else
			foundNode->parent->right = foundNode->left;
		delete foundNode;
	}
	//���� � ���� ������������ ������� ������
	else
	{
		if (foundNode->parent->left == foundNode)
			foundNode->parent->left = foundNode->right;
		else
			foundNode->parent->right = foundNode->right;
		delete foundNode;
	}

	return true;
}

template<typename Type>
void BinaryTree<Type>::addItemSubtree(Type item, TreeNode<Type>* subroot)
{
	if (item < subroot->data)
	{
		if (subroot->left == nullptr)
		{
			subroot->left = new TreeNode<Type>(item);
			subroot->left->parent = subroot;
			++size;
			return;
		}
		addItemSubtree(item, subroot->left);
	}
	else
	{
		if (subroot->right == nullptr)
		{
			subroot->right = new TreeNode<Type>(item);
			subroot->right->parent = subroot;
			++size;
			return;
		}
		addItemSubtree(item, subroot->right);
	}
}

template<typename Type>
void BinaryTree<Type>::deleteSubtree(TreeNode<Type>* subroot)
{
	if (subroot->left != nullptr)
		deleteSubtree(subroot->left);
	if (subroot->right != nullptr)
		deleteSubtree(subroot->right);
	delete subroot;
}

template<typename Type>
bool BinaryTree<Type>::findItemSubtree(Type item, TreeNode<Type>*& result, TreeNode<Type>* subroot)
{
	if (item == subroot->data)
	{
		result = subroot;
		return true;
	}
	if (item < subroot->data)
	{
		if (subroot->left == nullptr)
			return false;
		return findItemSubtree(item, result, subroot->left);
	}
	else
	{
		if (subroot->right == nullptr)
			return false;
		return findItemSubtree(item, result, subroot->right);
	}
}

template<typename Type>
void BinaryTree<Type>::printSubtree(TreeNode<Type>* subroot)
{
	if (subroot->left != nullptr)
		printSubtree(subroot->left);

	cout << subroot->data << endl;

	if (subroot->right != nullptr)
		printSubtree(subroot->right);
}