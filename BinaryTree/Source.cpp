#include <iostream>
#include "BinaryTree.h"
using namespace std;

int main()
{
	BinaryTree<int> tree;
	tree.addItem(5);
	tree.addItem(3);
	tree.addItem(2);
	tree.addItem(1);
	tree.addItem(6);
	tree.addItem(7);

	tree.print();

	int x;
	if (tree.findItem(0, x))
		cout << x << endl;
	else
		cout << "Not found" << endl;

	cout << endl;

	tree.removeItem(6);
	tree.print();

	system("pause");
	return 0;
}