#include <iostream>
#include "List.h"
using namespace std;

int main()
{
	List<int> list;
	list.push_back(1);
	list.push_back(2);
	list.push_front(3);
	cout << list.insert(1, 4) << endl;
	list.remove(2);
	cout << list << endl;
	int x;
	cout << list.pop_back(x) << endl;
	cout << x << endl;
	cout << list.pop_front(x) << endl;
	cout << x << endl;

	cout << list << endl;
	
	if (list.find(1, x))
		cout << "found: " << x << endl;

	list.push_back(10);
	list.push_back(15);
	//���������
	List<int>::Iterator curr = list.begin();
	do
	{
		cout << *curr << endl;
	} while (curr++ != list.end());

	system("pause");
	return 0;
}