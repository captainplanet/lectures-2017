#include <iostream>
#include <fstream>
#include <time.h>
using namespace std;

int main()
{
	srand(time(NULL));
	int rows = 5;
	int cols = 7;

	int readRows;
	int readCols;

	int** array2d = new int*[rows];

	for (int i = 0; i < rows; ++i)
	{
		array2d[i] = new int[cols];
		for (int j = 0; j < cols; ++j)
			array2d[i][j] = rand() % 10;
	}

	fstream out;
	out.open("out.txt");
	if (!out.is_open())
	{
		cout << "Out file error" << endl;
		system("pause");
		return -1;
	}

	//Writing size
	out << rows << " " << cols << endl;
	//Writing values
	for (int i = 0; i < rows; ++i)
	{
		for (int j = 0; j < cols; ++j)
			out << array2d[i][j] << " ";
		out << endl;
	}

	out.seekg(0, ios::beg);
	//1. File size
	//in.open("in.txt",ios::binary);
	//in.seekg(0,ios::end);
	//int size = out.tellg() + 1;
	//2. Read whole file
	//char* str = new char[size];
	//in.read(str,size);
	//out.write(str,size);
	

	//Reading size
	out >> readRows >> readCols;
	int** readArray = new int*[readRows];
	//Reading values
	for (int i = 0; i < readRows; ++i)
	{
		readArray[i] = new int[readCols];
		for (int j = 0; j < readCols; ++j)
		{
			out >> readArray[i][j];
			cout << readArray[i][j] << " ";
		}
		cout << endl;
	}

	for (int i = 0; i < readRows; ++i)
		delete[] readArray[i];
	delete[] readArray;

	for (int i = 0; i < rows; ++i)
		delete[] array2d[i];
	delete[] array2d;

	out.close();
	system("pause");
	return 0;
}