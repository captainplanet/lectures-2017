#include <iostream>
#include <time.h>
using namespace std;

int main()
{
	srand(time(NULL));
	int rows = 1;
	int cols = 1;
	int** leftMatrix;
	int** rightMatrix;
	int** resultMatrix;

	cout << "Input number of rows: ";
	cin >> rows;
	cout << "Input number of cols: ";
	cin >> cols;

	leftMatrix = new int*[rows];
	rightMatrix = new int*[rows];
	resultMatrix = new int*[rows];
	for (int i = 0; i < rows; ++i)
	{
		leftMatrix[i] = new int[cols];
		rightMatrix[i] = new int[cols];
		resultMatrix[i] = new int[cols];
		for (int j = 0; j < cols; ++j)
		{
			leftMatrix[i][j] = rand() % 21 - 10;
			rightMatrix[i][j] = rand() % 21 - 10;
			resultMatrix[i][j] = leftMatrix[i][j] + rightMatrix[i][j];
		}
	}

	cout << endl;
	//����� � ������� ������ leftMatrix
	for (int i = 0; i < rows; ++i)
	{
		for (int j = 0; j < cols; ++j)
			cout << leftMatrix[i][j] << '\t';
		cout << endl;
		delete[] leftMatrix[i];
	}
	delete[] leftMatrix;

	cout << endl;
	//����� � ������� ������ rightMatrix
	for (int i = 0; i < rows; ++i)
	{
		for (int j = 0; j < cols; ++j)
			cout << rightMatrix[i][j] << '\t';
		cout << endl;
		delete[] rightMatrix[i];
	}
	delete[] rightMatrix;

	cout << endl;
	//����� � ������� ������ resultMatrix
	for (int i = 0; i < rows; ++i)
	{
		for (int j = 0; j < cols; ++j)
			cout << resultMatrix[i][j] << '\t';
		cout << endl;
		delete[] resultMatrix[i];
	}
	delete[] resultMatrix;

	//������ ���������� ������� �������
	/*int* x1 = new int[3];
	int* tmp = new int[4];
	for (int i = 0; i < 3; ++i)
		tmp[i] = x1[i];
	delete[] x1;
	x1 = tmp;*/

	//��� delete[] � ����� �����
	//����������� ��������� ������,
	//�������� ������������
	/*for (int i = 0; i < 100; ++i)
	{
		int* x = new int[1024 * 1024 * 10];
		for (int j = 0; j < 2e7; ++j);
		cout << "yay" << endl;
	}*/

	system("pause");
	return 0;
}