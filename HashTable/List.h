#include <iostream>
using namespace std;

struct Band
{
	char name[64];
	int year;
	char genre[64];
	Band(char* _name, int _year, char* _genre)
	{
		strcpy_s(name, _name);
		year = _year;
		strcpy_s(genre, _genre);
	}
	Band() {}

	bool operator==(const Band& right)
	{
		return strcmp(name, right.name) == 0;
	}

	bool operator!=(const Band& right)
	{
		return strcmp(name, right.name) != 0;
	}

	Band& operator=(const Band& right)
	{
		strcpy_s(name, right.name);
		strcpy_s(genre, right.genre);
		year = right.year;
		return *this;
	}
};

template <typename Type>
struct ListNode{
	Type data;
	ListNode<Type>* next;

	ListNode() : next(nullptr) {}
	ListNode(Type& _data) : data(_data),
							next(nullptr) {}
	ListNode(Type& _data, ListNode<Type>* _next) : data(_data),
												next(_next) {}
};

template <typename Type>
class List{
private:
	int size;
	ListNode<Type>* first;
	ListNode<Type>* last;
	bool find(Type& data, ListNode<Type>*& elem);

public:
	List() : size(0), first(nullptr), last(nullptr) {}
	~List();

	void push_back(Type data);
	void push_front(Type data);
	bool insert(Type elem, Type data);

	bool pop_back(Type& data);
	bool pop_front(Type& data);
	bool remove(Type data);

	bool find(Type elem, Type& data);

	friend ostream& operator<<(ostream&, List<char*>&);
	friend ostream& operator<<(ostream&, List<Band>&);

	class Iterator
	{
	private:
		ListNode<Type>* node;
	public:
		Iterator() : node(nullptr) {}
		Iterator(ListNode<Type>* _node) : node(_node) {}
		Type& operator*() { return node->data; }
		Iterator operator++()
		{
			node = node->next;
			return *this;
		}
		Iterator operator++(int)
		{
			Iterator it(node);
			node = node->next;
			return it;
		}

		bool operator==(Iterator it) { return node == it.node; }
		bool operator!=(Iterator it) { return node != it.node; }
	};

	Iterator begin() { return Iterator(first); }
	Iterator end() { return Iterator(last); }
};

template<typename Type>
bool List<Type>::find(Type& data, ListNode<Type>*& elem)
{
	if (size == 0) return false;
	ListNode<Type>* curr = first;
	while (curr->data != data)
	{
		curr = curr->next;
		if (curr == nullptr)
			return false;
	}

	elem = curr;

	return true;
}

template <typename Type>
List<Type>::~List()
{
	if (size < 1) return;
	do 
	{
		ListNode<Type>* tmp = first;
		first = first->next;
		delete tmp;
	} while (first != nullptr);
}

template <typename Type>
void List<Type>::push_back(Type data)
{
	if (size == 0)
	{
		first = new ListNode<Type>(data);
		last = first;
	}
	else
	{
		ListNode<Type>* new_elem = new ListNode<Type>(data);
		last->next = new_elem;
		last = new_elem;
	}
	++size;
}

template <typename Type>
void List<Type>::push_front(Type data)
{
	if (size == 0)
	{
		first = new ListNode<Type>(data);
		last = first;
	}
	else
	{
		first = new ListNode<Type>(data, first);
	}
	++size;
}

template <typename Type>
bool List<Type>::insert(Type after, Type data)
{
	ListNode<Type>* curr;
	if (find(after, curr) == false) return false;
	ListNode<Type>* new_elem = new ListNode<Type>(data, curr->next);
	curr->next = new_elem;

	if (curr == last) last = new_elem;

	++size;
	return true;
}

template <typename Type>
bool List<Type>::pop_back(Type& data)
{
	if (size == 0) return false;
	data = last->data;
	--size;
	if (size == 0)
	{
		delete[] first;
		return true;
	}
	ListNode<Type>* prev = first;
	while (prev->next != last)
		prev = prev->next;
	delete last;
	prev->next = nullptr;
	last = prev;
	return true;
}

template <typename Type>
bool List<Type>::pop_front(Type& data)
{
	if (size == 0) return false;
	data = first->data;
	--size;
	ListNode<Type>* tmp = first;
	first = first->next;
	delete tmp;
	return true;
}

template<typename Type>
bool List<Type>::remove(Type data)
{
	ListNode<Type>* elem_to_remove;
	if (find(data, elem_to_remove) == false) return false;
	ListNode<Type>* prev = first;
	while (prev->next != elem_to_remove)
		prev = prev->next;
	prev->next = elem_to_remove->next;
	if (elem_to_remove == last)
		last = prev;

	delete elem_to_remove;
	--size;
	return true;
}

template <typename Type>
bool List<Type>::find(Type data, Type& elem)
{
	ListNode<Type>* elem_found;	
	if (find(data, elem_found) == false) return false;
	elem = elem_found->data;
	return true;
}

ostream& operator<<(ostream& out, List<char*>& list)
{
	if (list.size == 0) return out;

	ListNode<char*>* curr = list.first;
	while (curr != nullptr)
	{
		out << curr->data << " ";
		curr = curr->next;
	} 

	return out;
}

ostream& operator<<(ostream& out, List<Band>& list)
{
	if (list.size == 0) return out;

	ListNode<Band>* curr = list.first;
	while (curr != nullptr)
	{
		out << "{" << curr->data.name << ", " 
			<< curr->data.year << "} ";
		curr = curr->next;
	}

	return out;
}