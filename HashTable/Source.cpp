#include <iostream>
#include "HashTable.h"
using namespace std;

char* keyStringFunc(char* str)
{
	return str;
}

int hashStringFunc(char* str)
{
	return str[0] - 97;
}

char* keyBandFunc(Band band)
{
	int strLength = strlen(band.name) + 1;
	char * strcp = new char[strLength];
	strcpy_s(strcp, strLength, band.name);
	return strcp;
}

int hashBandFunc(char* key)
{
	return key[0] - 97;
}

int main()
{
	HashTable<Band, char*> table(26, hashBandFunc, keyBandFunc);
	table.insert(Band("ariya",1985,"heavy metal"));
	table.insert(Band("metallica",1981,"trash metal"));
	table.insert(Band("megadeth",1983,"trash metal"));
	table.insert(Band("slayer",1981,"trash metal"));
	table.insert(Band("anthrax",1981,"trash metal"));

	table.print();

	Band metallica("metallica", 0, "");
	Band result;
	cout << table.find(metallica, result) << endl;

	cout << result.name << endl;
	cout << result.year << endl;
	cout << result.genre << endl;

	system("pause");
	return 0;
}